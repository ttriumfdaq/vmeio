rm -rf db/ greybox_tmp/ incremental_db/ simulation/
cd output_files
rm -f *.rpt *.eqn *.qarlog *.qmsg *.pof *.summary *.jpg *.html *.smsg *.jdi *.pin *.done
cd ..


# find . -type d -name .svn -exec rm -rf {} \;
# ; terminates the exec cmdline and needs to be escaped to stop shell taking it
