#**************************************************************
# Time Information
#**************************************************************
set_time_format -unit ns -decimal_places 3

#**************************************************************
# Create Clocks
#**************************************************************
create_clock -name "CLK" -period 8.000ns [get_ports {LVDS_OSC1}]


# Automatically constrain PLL and other generated clocks
derive_pll_clocks -create_base_clocks

# Automatically calculate clock uncertainty to jitter and other effects.
derive_clock_uncertainty

#**************************************************************
# timing exceptions
#**************************************************************

# ignore timing failure on clk transfer synchronisers
# (they are there because they will fail timing)
#set_false_path -to {*|sync*}

# ignore timing problems with signaltap
set_false_path -to {sld_signaltap:*|*}

# reset timing is not important - can wait till nxt clk
#set_false_path -from {*|RSTn}
