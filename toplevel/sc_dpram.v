module sc_dpram #(parameter AWIDTH = 12, parameter DWIDTH = 32,  parameter DATAREG = "UNREGISTERED",
                  parameter INIT_FILE = "UNUSED",                parameter RWMODE = "DONT_CARE",  
                  parameter BENWIDTH = 1,                        parameter DEVICE  = "Arria V",
                  parameter NUMWORDS = 0                                                             ) (
   input              clock,  input                   wren,
   input  [DWIDTH-1:0] data,  input [AWIDTH-1:0] wraddress,   input [BENWIDTH-1:0] byte_ena,
   output [DWIDTH-1:0]    q,  input [AWIDTH-1:0] rdaddress
);

generate
if( BENWIDTH == 1 ) begin
   wire byte_enable = 1'b1;
end else begin
   wire [BENWIDTH-1:0] byte_enable = byte_ena;
end
endgenerate

localparam NWORDS  = (NUMWORDS == 0) ? (1<<AWIDTH) : NUMWORDS;

// other choices for parameters ...
//    operation_mode = "BIDIR_DUAL_PORT", "DUAL_PORT", "SINGLE_PORT", "ROM"
//    init_file = "test.mif", "UNUSED" [init-file not available for M-RAM]
altsyncram #( .INIT_FILE(INIT_FILE),   .lpm_type("altsyncram"),
   .clock_enable_input_a("BYPASS"),    .clock_enable_input_b("BYPASS"),
   .address_aclr_b("NONE"),            .address_reg_b("CLOCK0"),
   .outdata_aclr_b("NONE"),            .outdata_reg_b(DATAREG),
   .widthad_a(AWIDTH),                 .widthad_b(AWIDTH),
   .width_a(DWIDTH),                   .width_b(DWIDTH),
   .numwords_a(NWORDS),                .numwords_b(NWORDS),
   .intended_device_family(DEVICE),    .operation_mode("DUAL_PORT"),
   .clock_enable_output_b("BYPASS"),   .power_up_uninitialized("FALSE"),
   .width_byteena_a(BENWIDTH),         .read_during_write_mode_mixed_ports(RWMODE)
) altsyncram_component (
   .clock0(clock),         .clock1 (1'b1),
   .data_a(data),          .data_b ({DWIDTH{1'b1}}),
   .wren_a(wren),          .wren_b (1'b0),
   .address_a(wraddress),  .address_b (rdaddress),
   .q_a (),                .q_b (q),
   .aclr0 (1'b0),          .aclr1 (1'b0),
   .addressstall_a (1'b0), .addressstall_b (1'b0),
   .byteena_a (byte_enable),      .byteena_b (1'b1),
   .clocken0 (1'b1),       .clocken1 (1'b1),
   .clocken2 (1'b1),       .clocken3 (1'b1),
   .rden_a (1'b1),         .rden_b (1'b1),
   .eccstatus ()
);

endmodule
