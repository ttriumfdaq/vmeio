//binary to grey: (x>>1)^x   e.g for 12bit code ...  
//   {0,x[11:1]} ^ x[11:0]  =  {x[11], x[11]^x[10], ..., x[1]^x[0]}

module bin2grey #(parameter DWIDTH = 12) (input wire [DWIDTH-1:0] bin_in, output wire [DWIDTH-1:0] grey_out);

genvar i;

assign grey_out[DWIDTH-1] = bin_in[DWIDTH-1];
generate
for(i=DWIDTH-2; i>=0; i=i-1) begin : outbit
   assign grey_out[i] = bin_in[i] ^ bin_in[i+1];
end
endgenerate

endmodule
