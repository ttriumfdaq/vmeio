
module remote_update (
	busy,
	data_out,
	param,
	read_param,
	reconfig,
	reset_timer,
	clock,
	reset);	

	output		busy;
	output	[31:0]	data_out;
	input	[2:0]	param;
	input		read_param;
	input		reconfig;
	input		reset_timer;
	input		clock;
	input		reset;
endmodule
