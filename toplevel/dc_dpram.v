module dc_dpram #(parameter AWIDTH = 12,  parameter DATAREG = "UNREGISTERED", 
                    parameter DWIDTH = 32,  parameter DEVICE  = "Arria V" ) (
   input wrclock,  input  [DWIDTH-1:0] data,  input [AWIDTH-1:0] wraddress,  input wren,
   input rdclock,  output [DWIDTH-1:0]    q,  input [AWIDTH-1:0] rdaddress
);

altsyncram #(                          .lpm_type("altsyncram"),
   .clock_enable_input_a("BYPASS"),    .clock_enable_input_b("BYPASS"),
   .address_aclr_b("NONE"),            .address_reg_b("CLOCK1"),
   .outdata_aclr_b("NONE"),            .outdata_reg_b(DATAREG),
   .widthad_a(AWIDTH),                 .widthad_b(AWIDTH),
   .width_a(DWIDTH),                   .width_b(DWIDTH),
   .numwords_a(1<<AWIDTH),             .numwords_b(1<<AWIDTH),
   .intended_device_family(DEVICE),    .operation_mode("DUAL_PORT"),
   .clock_enable_output_b("BYPASS"),   .power_up_uninitialized("FALSE"),
   .width_byteena_a(1),                .read_during_write_mode_mixed_ports("DONT_CARE")
) altsyncram_component (
   .clock0(wrclock),       .clock1 (rdclock),
   .data_a(data),          .data_b ({DWIDTH{1'b1}}),
   .wren_a(wren),          .wren_b (1'b0),
   .address_a(wraddress),  .address_b (rdaddress),
   .q_a (),                .q_b (q),               
   .aclr0 (1'b0),          .aclr1 (1'b0),
   .addressstall_a (1'b0), .addressstall_b (1'b0),
   .byteena_a (1'b1),      .byteena_b (1'b1),
   .clocken0 (1'b1),       .clocken1 (1'b1),
   .clocken2 (1'b1),       .clocken3 (1'b1),
   .rden_a (1'b1),         .rden_b (1'b1),
   .eccstatus ()
);

endmodule
