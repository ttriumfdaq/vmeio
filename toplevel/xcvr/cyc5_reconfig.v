`timescale 1 ps / 1 ps
module cyc5_reconfig #(
      parameter device_family                 = "Cyclone V",
      parameter number_of_reconfig_interfaces = 2,
      parameter enable_offset                 = 1,
      parameter enable_lc                     = 0,
      parameter enable_dcd                    = 0,
      parameter enable_dcd_power_up           = 1,
      parameter enable_analog                 = 1,
      parameter enable_eyemon                 = 0,
      parameter enable_ber                    = 0,
      parameter enable_dfe                    = 0,
      parameter enable_adce                   = 0,
      parameter enable_mif                    = 0,
      parameter enable_pll                    = 0
   ) (
      output wire         reconfig_busy,
      input  wire         mgmt_clk_clk,
      input  wire         mgmt_rst_reset,
      input  wire [6:0]   reconfig_mgmt_address,
      input  wire         reconfig_mgmt_read,
      output wire [31:0]  reconfig_mgmt_readdata,
      output wire         reconfig_mgmt_waitrequest,
      input  wire         reconfig_mgmt_write,
      input  wire [31:0]  reconfig_mgmt_writedata,
      output wire [139:0] reconfig_to_xcvr,
      input  wire [91:0]  reconfig_from_xcvr
   );

   alt_xcvr_reconfig #(
      .device_family                 (device_family),
      .number_of_reconfig_interfaces (number_of_reconfig_interfaces),
      .enable_offset                 (enable_offset),
      .enable_lc                     (enable_lc),
      .enable_dcd                    (enable_dcd),
      .enable_dcd_power_up           (enable_dcd_power_up),
      .enable_analog                 (enable_analog),
      .enable_eyemon                 (enable_eyemon),
      .enable_ber                    (enable_ber),
      .enable_dfe                    (enable_dfe),
      .enable_adce                   (enable_adce),
      .enable_mif                    (enable_mif),
      .enable_pll                    (enable_pll)
   ) gx_cfg_inst (
      .reconfig_busy             (reconfig_busy),
      .mgmt_clk_clk              (mgmt_clk_clk),
      .mgmt_rst_reset            (mgmt_rst_reset),
      .reconfig_mgmt_address     (reconfig_mgmt_address),
      .reconfig_mgmt_read        (reconfig_mgmt_read),
      .reconfig_mgmt_readdata    (reconfig_mgmt_readdata),
      .reconfig_mgmt_waitrequest (reconfig_mgmt_waitrequest),
      .reconfig_mgmt_write       (reconfig_mgmt_write),
      .reconfig_mgmt_writedata   (reconfig_mgmt_writedata),
      .reconfig_to_xcvr          (reconfig_to_xcvr),
      .reconfig_from_xcvr        (reconfig_from_xcvr),
      .tx_cal_busy               (),
      .rx_cal_busy               (),
      .cal_busy_in               (1'b0),
      .reconfig_mif_address      (),
      .reconfig_mif_read         (),
      .reconfig_mif_readdata     (16'b0000000000000000),
      .reconfig_mif_waitrequest  (1'b0)
   );

endmodule
