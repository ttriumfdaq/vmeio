// monitor errors and realign when needed (e.g. cable reconnect)
module gx_realign #(parameter NCHAN = 4) (  
   input  wire [  NCHAN-1:0] clk,           input wire reset,
	output wire [  NCHAN-1:0] channel_good,  output reg  [  NCHAN-1:0] rx_enarealign,
	input  wire [4*NCHAN-1:0] rx_errdetect,  input  wire [4*NCHAN-1:0] rx_disperr,
   input  wire [4*NCHAN-1:0] rx_patdetect,  input  wire [4*NCHAN-1:0] rx_syncstatus,
   output reg  [  NCHAN-1:0] xcvr_reset
); // stratix4 and ArriaV word-aligners respond to +ve edge on rx_enarealign

genvar i;
generate
for(i=0; i<NCHAN; i=i+1) begin :realign_chan
   assign channel_good[i] = (realign_time == 24'hffffff);
	reg [15:0] ok_count; reg [15:0] err_count;  reg [23:0] realign_time;  reg [7:0] realign_count;
	always @ (posedge clk[i]) begin
	   if( reset ) begin
		   ok_count     <= 16'h0; err_count        <= 16'h0;  realign_count <=  8'h0;  xcvr_reset[i] <= 1'b0;
			realign_time <= 24'h0; rx_enarealign[i] <= 1'b0;   realign_time  <= 24'h0;
      end else begin
		   realign_time     <= (realign_time  != 24'hffffff) ? realign_time + 24'h1 : realign_time;  xcvr_reset[i] <= 1'b0;
         realign_count    <= (realign_count == 8'h0) ? 8'h0 : realign_count - 8'h1;
 	      rx_enarealign[i] <= (realign_count != 8'h0);  ok_count <= ok_count;  err_count <= err_count;
	      if( (rx_errdetect[4*i+3:4*i] == 4'h0) && (rx_disperr[4*i+3:4*i] == 4'h0) ) begin
	         ok_count <= ( ok_count == 16'hFFFF ) ? ok_count : ok_count + 16'h1;
		   	if( err_count != 16'h0 && ok_count >= 16'h3f ) begin // reduce error by 1 every 64 good words
		         err_count <= err_count - 16'h1;                   // => > 2% or so error rate will continually resync
		    	   ok_count <= 16'h0;                                //     (a few times per second)
	         end
	      end else begin
   	   	err_count <= ( err_count == 16'hFFFF ) ? err_count : err_count + 16'h1;  
		      if( err_count >= 16'hFFF0 ) begin                    // too many errors - resync
		   	   err_count <= 16'h0;  realign_count <= 8'hF;  realign_time <= 24'h0;
		      end
			end
	   end
	end
end
endgenerate

endmodule
