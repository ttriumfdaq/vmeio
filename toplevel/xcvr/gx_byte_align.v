// Only realign after reset
module gx_byte_align #(parameter RATE = 4) (  
   input wire   clk,  input  wire [9*RATE-1:0] data_in,
   input wire reset,  output wire [9*RATE-1:0] data_out
);
reg [9*RATE-1:0] prev_data; always @ (posedge clk) prev_data <= data_in;

localparam PATTERN_1 = {4'hf,32'hBCBCBCBC};
localparam PATTERN_2 = {4'h0,32'h50505050};
wire pattern_ok = (data_out == PATTERN_1) || (data_out == PATTERN_2);

assign data_out = (shift == 2'h0) ? {data_in[35:32],                  data_in[31:0]                 } :
                  (shift == 2'h1) ? {data_in[34:32],prev_data[35   ], data_in[23:0],prev_data[31:24]} :
                  (shift == 2'h2) ? {data_in[33:32],prev_data[35:34], data_in[15:0],prev_data[31:16]} :
                                    {data_in[   32],prev_data[35:33], data_in[ 7:0],prev_data[31: 8]};

reg aligned; reg [1:0] shift;  reg [7:0] count;
always @ (posedge clk) begin
   if( reset ) begin
      aligned <=    1'b0;  shift <=  2'h0;  count <=  8'h0;
   end else begin
      aligned <= aligned;  shift <= shift;  count <= count;
      if( aligned == 1'b0 ) begin
         if( pattern_ok ) begin
            count <= ( count < 8'h7f ) ? count + 8'h1 : count;
            if( count == 8'h7f ) aligned <= 1'b1; 
         end else begin
            shift <= shift + 2'h1;  count <= 0;
         end
      end
   end
end

endmodule
