module ipv4_chksum( input wire clk, input wire [159:0] data, output wire [15:0] chksum );

reg [33:0] part_sum32;
always @ (posedge clk) begin
   part_sum32 <= data[159:128] + data[127:96] + data[95:64] + data[63:32] + data[31: 0]; // 34bit sum
end

wire [31:0] part_sum16 = part_sum32[33:32] + part_sum32[31:16] + part_sum32[15:0];
assign chksum = ~( part_sum16[31:16] + part_sum16[15:0]  );

endmodule
