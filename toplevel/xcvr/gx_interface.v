// only hold RX side in reset if channel bad (to avoid grabbing junk data)
// TX side should continue sending proper patterns
module gx_interface #(parameter NCHAN=4, parameter RATE=1) ( input wire [NCHAN-1:0] reset,
	input  wire                    rx_outclk,   input  wire                    tx_inclk,   // fpga side      rx is out, tx is in
   input  wire [     4*NCHAN-1:0] tx_ctrlin,   input  wire [    32*NCHAN-1:0] tx_datain,  // ---------
   output wire [     4*NCHAN-1:0] rx_ctrlout,  output wire [    32*NCHAN-1:0] rx_dataout, //                single clocks, full width busses

   input  wire [       NCHAN-1:0] rx_inclk,    input  wire [       NCHAN-1:0] tx_outclk,  // xcvr side      rx is in, tx is out
   input  wire [  RATE*NCHAN-1:0] rx_ctrlin,   input  wire [8*RATE*NCHAN-1:0] rx_datain,  // ---------
   output wire [  RATE*NCHAN-1:0] tx_ctrlout,  output wire [8*RATE*NCHAN-1:0] tx_dataout   //                clock per chan, rate width busses
);
// arriaV builtin ratematch is 20 words deep and uses 20bit pattern (10bit ctrl + 10bit skip/insert)
//        *slip/ins must be neutral disparity*
genvar i;
generate
if( RATE == 1 ) begin 
   localparam PATTERN = {1'b0,8'h50};
   localparam PREV_PAT= {1'b1,8'hBC};

   for(i=0; i<NCHAN; i=i+1) begin :if_chan
      ratematch_fifo #( .DWIDTH(9*RATE), .AWIDTH(3), .PATTERN(PATTERN), .PREV_PAT(PREV_PAT) ) rx4g_fifo_in ( .reset(reset[i]), // RX
         .wrclk(rx_inclk[i]),  .data_in ({rx_ctrlin [                  i], rx_datain [ 8*i+7:  8*i]}),    // no wr/rd enables as both permanently set
         .rdclk(rx_outclk),    .data_out({rx_ctrlout[                4*i], rx_dataout[32*i+7: 32*i]})
      );
      phasematch_fifo #( .DWIDTH(9*RATE) )  tx4g_fifo_out ( .reset(1'b0),                                 // TX
			.rdclk(tx_outclk[i]),  .data_out({tx_ctrlout[                  i], tx_dataout[ 8*i+7:  8*i]}),
         .wrclk(tx_inclk    ),  .data_in ({tx_ctrlin [                4*i], tx_datain [32*i+7: 32*i]}) 
      );
		assign rx_dataout[32*i+31:32*i+8] = 24'h0;
      assign rx_ctrlout[ 4*i+ 3: 4*i+1] = 3'h0;
   end
end else if( RATE == 2 ) begin
   localparam PATTERN = {4'h0,32'h50505050};
   localparam PREV_PAT= {4'hf,32'hBCBCBCBC};
   localparam REALIGN_PATTERN= {4'hf,32'hBCBCBCBC};
   wire [ 4*NCHAN-1:0] rx_al_ctrlout; 
   wire [32*NCHAN-1:0] rx_al_dataout;

   // realign before ratematch to allow skipping whole columns
   // ratematch also needs modifying, to skip whole columns, not individual lanes
   //  => would be best to combine these two modules - add realign part to multi-lane ratematch
   
   // 4 lanes of 16bits
   realign_control #( .DWIDTH(9*RATE*NCHAN), .AWIDTH(3), .PATTERN(REALIGN_PATTERN) ) al4g_fifo_in ( .clk(rx_inclk),  .reset(|reset),  
      .data_in ({    rx_ctrlin[ 1],     rx_datain[ 15:  8],     rx_ctrlin [0],     rx_datain[  7:  0],   // 9*rate bits
                     rx_ctrlin[ 3],     rx_datain[ 31: 24],     rx_ctrlin[ 2],     rx_datain[ 23: 16],   //   "
                     rx_ctrlin[ 5],     rx_datain[ 47: 40],     rx_ctrlin[ 4],     rx_datain[ 39: 32],   //   "
                     rx_ctrlin[ 7],     rx_datain[ 63: 56],     rx_ctrlin[ 6],     rx_datain[ 55: 48]}), //   "
      .data_out({rx_al_ctrlout[ 1], rx_al_dataout[ 15:  8], rx_al_ctrlout[ 0], rx_al_dataout[  7:  0],   // total in  = 4*9*rate
                 rx_al_ctrlout[ 5], rx_al_dataout[ 47: 40], rx_al_ctrlout[ 4], rx_al_dataout[ 39: 32],   // total out =    "
                 rx_al_ctrlout[ 9], rx_al_dataout[ 79: 72], rx_al_ctrlout[ 8], rx_al_dataout[ 71: 64],
                 rx_al_ctrlout[13], rx_al_dataout[111:104], rx_al_ctrlout[12], rx_al_dataout[103: 96]})
   );
   for(i=0; i<NCHAN; i=i+1) begin :if_chan
      ratematch_fifo #( .DWIDTH(9*RATE), .AWIDTH(3), .PATTERN(PATTERN) ) rx4g_fifo_in ( .reset(reset[i]), // RX
         .wrclk(rx_inclk[i]),  .data_in ({rx_al_ctrlout[         2*i+1:2*i], rx_al_dataout [16*i+15: 16*i]}),
         .rdclk(rx_outclk ),   .data_out({rx_ctrlout[            4*i+1:4*i], rx_dataout[    32*i+15: 32*i]})
      );
      phasematch_fifo #( .DWIDTH(9*RATE) )  tx4g_fifo_out ( .reset(1'b0),                                    // TX
         .rdclk(tx_outclk[i]),  .data_out({tx_ctrlout[           2*i+1:2*i], tx_dataout[16*i+15: 16*i]}),
         .wrclk(tx_inclk    ),  .data_in ({tx_ctrlin  [          4*i+1:4*i], tx_datain [32*i+15: 32*i]})
      );
      assign rx_dataout[32*i+31:32*i+16] = 16'h0;
      assign rx_ctrlout[ 4*i+ 3: 4*i+ 2] = 2'h0;
   end
end else begin // RATE=4
   wire [  RATE*NCHAN-1:0] rx_ctrlalign;  wire [8*RATE*NCHAN-1:0] rx_dataalign;
   localparam PATTERN = {4'h0,32'h50505050};
   localparam PREV_PAT= {4'hf,32'hBCBCBCBC};
   for(i=0; i<NCHAN; i=i+1) begin :if_chan
      gx_byte_align #( .RATE(RATE) ) ba (
          .clk(rx_inclk[i]),  .data_in ({rx_ctrlin   [RATE*(i+1)-1:RATE*i], rx_datain   [8*RATE*(i+1)-1:8*RATE*i]}),
          .reset(reset[i]),   .data_out({rx_ctrlalign[RATE*(i+1)-1:RATE*i], rx_dataalign[8*RATE*(i+1)-1:8*RATE*i]})
      );
      ratematch_fifo #( .DWIDTH(9*RATE), .AWIDTH(3), .PATTERN(PATTERN), .PREV_PAT(PREV_PAT) ) rx4g_fifo_in ( .reset(reset[i]), // RX
         .wrclk(rx_inclk[i]),  .data_in ({rx_ctrlalign[RATE*(i+1)-1:RATE*i], rx_dataalign[8*RATE*(i+1)-1:8*RATE*i]}),    // no wr/rd enables as both permanently set
         .rdclk(rx_outclk ),   .data_out({rx_ctrlout  [RATE*(i+1)-1:RATE*i], rx_dataout  [8*RATE*(i+1)-1:8*RATE*i]})
      );
      phasematch_fifo #( .DWIDTH(9*RATE) )  tx4g_fifo_out ( .reset(1'b0),                                     // TX
         .rdclk(tx_outclk[i]),  .data_out({tx_ctrlout[RATE*(i+1)-1:RATE*i], tx_dataout[8*RATE*(i+1)-1:8*RATE*i]}),
         .wrclk(tx_inclk    ),  .data_in ({tx_ctrlin [RATE*(i+1)-1:RATE*i], tx_datain [8*RATE*(i+1)-1:8*RATE*i]})
      );
   end
end
endgenerate

endmodule
