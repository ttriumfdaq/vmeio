// to simplify other modules, this module IO widths are all for rate=4 (gaps filled with zero if rate=1)
// comment this line to fail fit:    wire [255:0] gx_RxDataSas;   wire [175:0] gx_TxDataSas;
module gx_cyclone5_block #( parameter NCHAN = 1, parameter RATE = 1,  parameter POLINV = 0) (input wire reset,
   input  wire                xcvr_refclk, input  wire                dataio_clk,   input wire reconfig_clk,
   input  wire [32*NCHAN-1:0] tx_datain,   output wire [32*NCHAN-1:0] rx_dataout,
   input  wire [ 4*NCHAN-1:0] tx_ctrlen,   output wire  [4*NCHAN-1:0] rx_ctrldet,
   input  wire    [NCHAN-1:0] FMCX_RX,     output wire    [NCHAN-1:0] FMCX_TX,
   output wire    [NCHAN-1:0] channel_good
); // rate=4 => (32bits+4ctrl)/chan/clk  -> 32+4 at xcvr 
   // rate=2     16    +2                -> xcvr only has 16+2bits  ,  tx_in and rx_out have 31:16 = zero
   // rate=1      8    +1                -> xcvr only has  8+1bits  ,  tx_in and rx_out have 31: 8 = zero
   
// interface includes all rate/phasematching from inclks to outclks
// xcvr data on either tx_outclk by altera ratematch, or rx_clkout
gx_interface #(.NCHAN(NCHAN), .RATE(RATE)) gx_if (  .reset(~channel_good),
   .rx_outclk(  dataio_clk),  .tx_inclk(   dataio_clk),   // don't have ratematch on tx, so currently ...
   .rx_ctrlout( rx_ctrldet),  .rx_dataout( rx_dataout),   // ** must use dataio_clk as xcvr_refclk **
   .tx_ctrlin(   tx_ctrlen),  .tx_datain(   tx_datain),   //  ** (or clk derived from same crystal) **
	.rx_inclk(    rx_clkout),  .tx_outclk(   tx_clkout),   // if we want separate xcvr_refclk ...
   .rx_ctrlin(gx_RxCtrlDet),  .rx_datain(gx_RxDataOut),   // add ratematch between dataio_clk and tx_clkout (tx_clkout derived from refclk)
   .tx_ctrlout(gx_TxCtrlEn),  .tx_dataout(gx_TxDataIn)    // and then can use xcvr_refclk as pll/cal_blk inclk on xcvrs
);                                                        // *this clk also used for realign and reset*
// wire xcvr_refclk  = dataio_clk;
// currently using sfpl or sfp2 for refclk and sfp1 for dataio (the tx phasematch is enough for this to work)

wire [8*RATE*NCHAN-1:0] gx_TxDataIn;   wire [8*RATE*NCHAN-1:0] gx_RxDataOut;
wire [  RATE*NCHAN-1:0] gx_TxCtrlEn;   wire [  RATE*NCHAN-1:0] gx_RxCtrlDet;
wire [       NCHAN-1:0] tx_clkout;     wire [       NCHAN-1:0] rx_clkout;

wire    [NCHAN-1:0]    rx_patalign /* synthesis keep */;  wire   [NCHAN-1:0] xcvr_reset;
wire    [NCHAN-1:0]     rx_byteord /* synthesis keep */;  wire   [NCHAN-1:0] rx_byteord_ok    /* synthesis keep */;
wire  [4*NCHAN-1:0]      rx_patdet /* synthesis keep */;  wire [4*NCHAN-1:0] rx_syncstatus    /* synthesis keep */;
wire  [4*NCHAN-1:0] rx_disperr_out /* synthesis keep */;  wire [4*NCHAN-1:0] rx_errdetect_out /* synthesis keep */;
// monitor errors (only rx_errdetect, rx_disperr atm) and realign when needed (e.g. cable reconnect)
gx_realign #(.NCHAN(NCHAN)) gxrealign ( .reset(       ~pll_locked||reset),  .channel_good(channel_good),
   .clk(                  rx_clkout),   .rx_enarealign(      rx_patalign),  .xcvr_reset(xcvr_reset),
   .rx_errdetect(  rx_errdetect_out),   .rx_disperr(      rx_disperr_out),
   .rx_patdetect(         rx_patdet),   .rx_syncstatus(    rx_syncstatus)    // should keep count of realignments
); // *after rxdigitalreset falls, alignemnt is done once automatically even without setting rx_std_wa_patternalign*
// there is also a register rx_enapatternalign which does the same as rx_std_wa_patternalign
//  ^ this is for the "custom-phy" avalon-wrapped xcvr-interface with processor accessible registers
//  (module below is the native-phy "bare" xcvr interface which does not have these registers)
wire [NCHAN-1:0] pll_locked;  wire [NCHAN-1:0] pll_powerdown;  wire [NCHAN-1:0] rx_lockedtodata;

cyc5_xcvr /*#(.NCHAN(NCHAN), .RATE(RATE), .POLINV(POLINV))*/ gx_inst (
   .tx_pll_refclk(          xcvr_refclk), .rx_cdr_refclk(          xcvr_refclk),
   .tx_std_coreclkin(         tx_clkout), .rx_std_coreclkin(         rx_clkout),  // *if ratematch enabled, need both as txclkout*
   .tx_std_clkout(            tx_clkout), .rx_std_clkout(            rx_clkout),
   .pll_locked(              pll_locked), .pll_powerdown(        pll_powerdown),
   .tx_analogreset(      tx_analogreset), .rx_analogreset(      rx_analogreset),
	.tx_digitalreset(    tx_digitalreset), .rx_digitalreset(    rx_digitalreset),
   .tx_cal_busy(            tx_cal_busy), .rx_cal_busy(            rx_cal_busy),
   .reconfig_to_xcvr(          gx_cfgin), .reconfig_from_xcvr(       gx_cfgout),
	.tx_serial_data(             FMCX_TX), .rx_serial_data(             FMCX_RX),
   .tx_parallel_data(    gx_RawTxDataIn), .rx_parallel_data(   gx_RawRxDataOut)
   //.rx_is_lockedtodata( rx_lockedtodata), .rx_std_wa_patternalign( rx_patalign),
   //.rx_std_byteorder_ena(    rx_byteord), .rx_std_byteorder_flag(rx_byteord_ok)
); // manual for the above module is in V-series xcvr phy user guide (xcvr_user_guide.pdf)

wire [NCHAN-1:0] rx_digitalreset;  wire [NCHAN-1:0] rx_analogreset;  wire [NCHAN-1:0] rx_cal_busy;  wire [NCHAN-1:0] rx_ready;
wire [NCHAN-1:0] tx_digitalreset;  wire [NCHAN-1:0] tx_analogreset;  wire [NCHAN-1:0] tx_cal_busy;  wire [NCHAN-1:0] tx_ready;
cyc5_reset /*#(.num_chan(NCHAN))*/ cyc5_reset_inst (
   .clock(              xcvr_refclk),      .reset(reset||xcvr_reset[0]),
   .pll_locked(          pll_locked), .pll_powerdown(    pll_powerdown),
   .tx_analogreset(  tx_analogreset), .rx_analogreset(  rx_analogreset),
	.tx_digitalreset(tx_digitalreset), .rx_digitalreset(rx_digitalreset),
   .tx_cal_busy(        tx_cal_busy), .rx_cal_busy(        rx_cal_busy),
   .tx_ready(              tx_ready), .rx_ready(              rx_ready), // both unused
   .pll_select(/*log2(NCHAN)*/ 4'h0), .rx_is_lockedtodata({NCHAN{|(rx_lockedtodata)}})
); // **entire block resets if ANY rx_is_lockedtodata go low**  =>  if some cables unattached,
   //    do not use unused channels lock signals or they'll reset the in-use channels
wire reconfig_busy;  wire [70*2*NCHAN-1:0] gx_cfgin;  wire [46*2*NCHAN-1:0] gx_cfgout; // 70 and 46 bits per interface
cyc5_reconfig /*#(.num_iface(NCHAN*2))*/ cyc5_reconfig_inst ( // 2 interfaces per channel: TX and PLL
   .mgmt_clk_clk(       xcvr_refclk), .mgmt_rst_reset(            reset),
   .reconfig_to_xcvr(      gx_cfgin), .reconfig_from_xcvr(    gx_cfgout),
	.reconfig_busy(    reconfig_busy)
);

/////////////////////  xcvr bus bit packing/unpacking   ////////////////////////////

// xcvr_io: the bits for up to rate=4 always exist, even if unused, i.e. always 64/44 bits per channel
// 32+4bit Rx: the 36 valid bits out of each 64 are [0-8,16-24,32-40,48-56] [64-71,] [128-135,] [192-200,208-216,224-232,240-248]
// 32+4bit Tx: the 36 valid bits out of each 44 are [0-8,11-19,22-30,33-41] [44-51,] [ 88- 95,] [132-140,143-151,154-162,165-173]
// followed by 9:rx_errdetect, 10:rx_syncstatus, 11:rx_disperr, 12:rx_patndetct, [14..13]:rx_rmfifostat, 15:rx_runningdisp
//   **for 16bt modes, valid bits are 0-8, 22-30or32-40** i.e. group 0,2 not 0,1 **
//
// the channel in use is gx_bits 132-40,192-200 i.e. xcvr chan3 in 0-3
// these map to bits 31-24 for rate1 or 127-96 for rate4

wire [44*NCHAN-1:0] gx_RawTxDataIn;   wire [64*NCHAN-1:0] gx_RawRxDataOut;
genvar i,j;
generate
for(j=0; j<NCHAN; j=j+1) begin :nchantx_assign
   for(i=0; i<RATE;  i=i+1) begin :ratetx_assign
	   localparam k = j*4   +i;    // output parameter offset    // allow space for rate=4
	   localparam q = j*RATE+i;    // GxData I/O      offsets    // corrected in interface
      if( RATE == 2 ) begin
	      localparam r = 64*j + 32*i; // gx rx parameter offset
	      localparam t = 44*j + 22*i; // gx tx parameter offset
      end else begin
	      localparam r = 64*j + 16*i; // gx rx parameter offset
	      localparam t = 44*j + 11*i; // gx tx parameter offset
      end
      assign gx_RawTxDataIn [t+10     :  t] = {2'h0,gx_TxCtrlEn[q],gx_TxDataIn[q*8+7:q*8]};
      assign gx_RxDataOut   [8*(q+1)-1:8*q] =  gx_RawRxDataOut[r+ 7:r];
      assign gx_RxCtrlDet     [q]           =  gx_RawRxDataOut[r+ 8];
      assign rx_errdetect_out [k]           =  gx_RawRxDataOut[r+ 9];
      assign rx_syncstatus    [k]           =  gx_RawRxDataOut[r+10];
      assign rx_disperr_out   [k]           =  gx_RawRxDataOut[r+11];
      assign rx_patdet        [k]           =  gx_RawRxDataOut[r+12];
   end
end
endgenerate

endmodule
