//
// vmeio control registers
//

`default_nettype none
module vmeio_regs
  (
   input wire 	      clk,
   input wire 	      reset,

   // controls and configurations

   input wire [15:0]  addr,
   input wire [31:0]  write_data,
   input wire         write_strobe,
   input wire         read_rdack,
   input wire         read_strobe,
   output wire [31:0] read_data,

   // data input
   
   input wire [31:0]  sof_revision_in,
   //input wire [31:0]  switches_in,
   //input wire [31:0]  buttons_in,
   input wire [15:0]  flash_programmer_in,
   input wire [31:0]  ecl_in_async,
   input wire [31:0]  lemo_in_async,
   //input wire [31:0]  gpio_in,
   input wire [31:0]  reg7_test_in,
   input wire [31:0]  reg8_scaler_data_in,
   input wire [31:0]  regF_input_num_in,
   input wire [31:0]  reg10_fifo_status,
   input wire [31:0]  reg11_fifo_data,
   input wire [31:0]  reg18_fc_ext_clk_100_counter,
   input wire [31:0]  reg19_fc_ext_clk_ext_counter,
   input wire [31:0]  reg1A_fc_ts_clk_100_counter,
   input wire [31:0]  reg1B_fc_ts_clk_ts_counter,
   input wire [31:0]  reg1C_ts_clk_pll_status,

   // controls out

   output reg 	      zero_scalers_out,
   output reg 	      latch_scalers_out,
   output reg  [15:0] scaler_addr_out,
   output reg         reset_pulse_out,
   //output reg         reconfig_pulse_out,
   output reg         reconfig_out,
   output reg         fifo_rdreq_out,
   output reg         ts_clk_pll_extswitch_out,
   output reg         sync_arm_out,
   output reg         sync_out,
   
   // registers

   //output reg [31:0]  reg0, // sof_revision
   output reg [31:0]  reg2_led_ctrl_out, // LED control
   output reg [31:0]  reg3_fp_led_out, // FP LED output
   output reg [31:0]  reg4_test,
   output reg [31:0]  reg5_rgb_led_out, // RGB LED output
   output reg [15:0]  reg8_flash_programmer_out,
   output reg [31:0]  reg11_lemo_out,
   //output reg [31:0]  regC_gpio_out,
   //output reg [31:0]  regD_out_enable_out,
   //output reg [31:0]  reg12_out,
   //output reg [31:0]  reg13_out,
   //output reg [31:0]  reg14_out,
   //output reg [31:0]  reg15_out,
   //output reg [31:0]  reg16_out,
   //output reg [31:0]  reg17_out,
   //output reg [31:0]  reg1D_lemo_out_mux_out,
   //output reg [31:0]  reg1E_cb_sync_a_out,
   //output reg [31:0]  reg1F_cb_sync_b_out,
   output reg [31:0]  reg_last
   );

   assign reg_last = 32'b0;

   //reg [31:0]         reg4_test;
   //reg [31:0]         regE;
   reg [31:0]         command;
   reg                command_set;
   reg                command_set1;
   wire               command_pulse = command_set & !command_set1;

   assign reset_pulse_out    = command_pulse & (command == 32'd1); // IO32_CMD_RESET
   //assign reconfig_pulse_out = command_pulse & (command == 32'd2); // IO32_CMD_FPGA_RECONFIGURE
   assign reconfig_out = (command == 32'd2); // IO32_CMD_FPGA_RECONFIGURE
   
   always_ff @ (posedge clk or posedge reset) begin
      if (reset) begin
	 read_data <= 32'b0;
         command   <= 32'b0;
	 //reg1_led_out <= 32'b0;
         command_set <= 0;
         command_set1 <= 0;
      end else if (write_strobe) begin // write enable
         command_set1 <= command_set;
	 case (addr[15:0])
	   16'h0000: begin
	      // latch_scalers_out <= write_data[0];
	      // zero_scalers_out <= write_data[1];
	      // fifo_rdreq_out <= write_data[2];
              // ts_clk_pll_extswitch_out <= (ts_clk_pll_extswitch_out & !write_data[3]) | write_data[4];
              // sync_arm_out <= write_data[5];
              // sync_out <= write_data[6];
	   end
	   16'd1: begin
              command <= write_data;
              command_set <= 1;
           end
	   16'd2: reg2_led_ctrl_out <= write_data;
	   16'd3: reg3_fp_led_out <= write_data;
	   16'd4: reg4_test <= write_data;
	   16'd5: reg5_rgb_led_out <= write_data;
	   //16'h0006: ;
	   //16'h0007: ;
	   16'd8: reg8_flash_programmer_out <= write_data[15:0];
	   //16'h0008: scaler_addr_out <= write_data[15:0];
	   // 9 is lemo in
	   // A is gpio in
	   16'd11: reg11_lemo_out <= write_data;
	   //16'h000C: regC_gpio_out <= write_data;
	   //16'h000D: regD_out_enable_out <= write_data;
	   //16'h000E: regE <= write_data;
	   // F is number of inputs
           // 10 is FIFO
           // 11 is FIFO
	   //16'h0012: reg12_out <= write_data;
	   //16'h0013: reg13_out <= write_data;
	   //16'h0014: reg14_out <= write_data;
	   //16'h0015: reg15_out <= write_data;
	   //16'h0016: reg16_out <= write_data;
	   //16'h0017: reg17_out <= write_data;
           // 18 clock counters
           // 19 clock counters
           // 1A clock counters
           // 1B clock counters
           // 1C pll_status
           //16'h001D: reg1D_lemo_out_mux_out <= write_data;
           //16'h001E: reg1E_cb_sync_a_out <= write_data;
           //16'h001F: reg1F_cb_sync_b_out <= write_data;
	   default: ;
	 endcase
	 read_data <= write_data;
      end else if (read_strobe) begin // read enable
	 case (addr[15:0])
	   16'd0: read_data <= sof_revision_in;
	   16'd1: read_data <= command;
	   //16'h0001: read_data <= reg1_led_out;
	   16'd2: read_data <= reg2_led_ctrl_out;
	   16'd3: read_data <= reg3_fp_led_out;
	   16'd4: read_data <= reg4_test;
	   16'd5: read_data <= reg5_rgb_led_out;
	   16'd6: read_data <= ecl_in_async;
	   //16'h0007: read_data <= reg7_test_in;
	   16'd8: read_data <= { flash_programmer_in, flash_programmer_in };
	   //16'h0008: begin
	   // scaler_addr_out <= write_data[15:0];
	   // read_data <= reg8_scaler_data_in;
	   //end
	   16'd9: read_data <= lemo_in_async;
	   //16'h000A: read_data <= gpio_in;
	   16'd11: read_data <= reg11_lemo_out;
	   //16'h000C: read_data <= regC_gpio_out;
	   //16'h000D: read_data <= regD_out_enable_out;
	   //16'h000E: read_data <= regE;
	   //16'h000F: read_data <= regF_input_num_in;
	   //16'h0010: read_data <= reg10_fifo_status;
	   //16'h0011: read_data <= reg11_fifo_data;
	   //16'h0012: read_data <= reg12_out;
	   //16'h0013: read_data <= reg13_out;
	   //16'h0014: read_data <= reg14_out;
	   //16'h0015: read_data <= reg15_out;
	   //16'h0016: read_data <= reg16_out;
	   //16'h0017: read_data <= reg17_out;
           //16'h0018: read_data <= reg18_fc_ext_clk_100_counter;
           //16'h0019: read_data <= reg19_fc_ext_clk_ext_counter;
           //16'h001A: read_data <= reg1A_fc_ts_clk_100_counter;
           //16'h001B: read_data <= reg1B_fc_ts_clk_ts_counter;
           //16'h001C: read_data <= reg1C_ts_clk_pll_status;
           //16'h001D: read_data <= reg1D_lemo_out_mux_out;
           //16'h001E: read_data <= reg1E_cb_sync_a_out;
           //16'h001F: read_data <= reg1F_cb_sync_b_out;
	   default: read_data <= 32'b0;
	 endcase
         command_set <= 0;
      end else begin
	 read_data <= 32'b0;
         command_set <= 0;
      end
   end // always_ff @ (posedge clk or posedge reset)

endmodule
