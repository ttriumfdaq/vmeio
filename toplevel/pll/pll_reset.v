// reset is generated 40-80ms after pll locks (once on powerup)
// reset_in resets pll and then generates above reset
// poweron_time is time since pll locked after last reset or power on
module pll_reset(  input wire autoreset,
   input wire inclk,      input wire reset_req,       output reg RSTn,
   output wire io_clk,   output wire sys_clk,   output wire fast_clk,
   output wire sys_locked, output reg [29:0] poweron_time
);

// reset_req is registered on ethernet_clk, and only lasts 1 clk => care required transferring to another clk
// - have to transfer, because as soon as the pll is reset, eth_clk freezes, which would hold reset on
//   so stretch to 4 ioclks -> 32ns duration, and re-register on input clk
reg [3:0] reset_req_delay; always @ (posedge io_clk) reset_req_delay <= {reset_req_delay[2:0],reset_req};
reg reset; always @ (posedge inclk) reset <= |(reset_req_delay);

reg reset_done;
always @ (posedge inclk or posedge reset) begin
   if( reset ) begin
      reset_done <= 1'b0; poweron_time <= 30'h0; // 1Gig -> 8 seconds @ 125Mhz, 10 seconds @ 100
   end else begin
      RSTn <= 1'b1; reset_done <= reset_done; poweron_time <= poweron_time + 30'h1;
      if( sys_locked  == 0                          ) poweron_time <= 30'h0; // keep at zero till locked
      if( poweron_time[21] && reset_done == 0       ) RSTn         <=  1'b0; // reset     at 40ms
      if( poweron_time[22]                          ) reset_done   <=  1'b1; // end reset at 80ms
      if( poweron_time == 30'h3fffffff && autoreset ) reset_done   <=  1'b0; // re-arm for 10s later
   end
end

pll_gen #( .NCLK(3), .REF("125.0 MHz"), .CLK1("125.0 MHz"), .CLK2("50.0 MHz") , .CLK3("200.0 MHz") ) pll_main  (
   .refclk(inclk),  .rst(1'b0),  .outclk0(io_clk),  .outclk1(sys_clk),  .outclk2(fast_clk),  .locked(sys_locked)
);

endmodule

