module pll_gen #(parameter NCLK = 1,           parameter REF  = "100.0 MHz",
                 parameter CLK1 = "100.0 MHz", parameter CLK2 = "0 MHz",
                 parameter CLK3 = "0 MHz",     parameter CLK4 = "0 MHz",
                 parameter CLK5 = "0 MHz",     parameter CLK6 = "0 MHz"  ) (
	input  wire refclk,  input  wire rst,
	output wire outclk0, output wire outclk1,
	output wire outclk2, output wire outclk3,
	output wire outclk4, output wire outclk5,
	output wire outclk6, output wire locked
);

altera_pll #(
   .reference_clock_frequency(REF),   .fractional_vco_multiplier("false"),
   .number_of_clocks(NCLK),           .operation_mode("direct"),
   .pll_type("General"),              .pll_subtype("General"),
   .output_clock_frequency0(   CLK1 ),.phase_shift0( "0 ps"),.duty_cycle0( 50),
   .output_clock_frequency1(   CLK2 ),.phase_shift1( "0 ps"),.duty_cycle1( 50),
   .output_clock_frequency2(   CLK3 ),.phase_shift2( "0 ps"),.duty_cycle2( 50),
   .output_clock_frequency3(   CLK4 ),.phase_shift3( "0 ps"),.duty_cycle3( 50),
   .output_clock_frequency4(   CLK5 ),.phase_shift4( "0 ps"),.duty_cycle4( 50),
   .output_clock_frequency5(   CLK6 ),.phase_shift5( "0 ps"),.duty_cycle5( 50),
   .output_clock_frequency6( "0 MHz"),.phase_shift6( "0 ps"),.duty_cycle6( 50),
   .output_clock_frequency7( "0 MHz"),.phase_shift7( "0 ps"),.duty_cycle7( 50),
   .output_clock_frequency8( "0 MHz"),.phase_shift8( "0 ps"),.duty_cycle8( 50),
   .output_clock_frequency9( "0 MHz"),.phase_shift9( "0 ps"),.duty_cycle9( 50),
   .output_clock_frequency10("0 MHz"),.phase_shift10("0 ps"),.duty_cycle10(50),
   .output_clock_frequency11("0 MHz"),.phase_shift11("0 ps"),.duty_cycle11(50),
   .output_clock_frequency12("0 MHz"),.phase_shift12("0 ps"),.duty_cycle12(50),
   .output_clock_frequency13("0 MHz"),.phase_shift13("0 ps"),.duty_cycle13(50),
   .output_clock_frequency14("0 MHz"),.phase_shift14("0 ps"),.duty_cycle14(50),
   .output_clock_frequency15("0 MHz"),.phase_shift15("0 ps"),.duty_cycle15(50),
   .output_clock_frequency16("0 MHz"),.phase_shift16("0 ps"),.duty_cycle16(50),
   .output_clock_frequency17("0 MHz"),.phase_shift17("0 ps"),.duty_cycle17(50)
) altera_pll_i (
   .rst (rst),        .locked (locked),
   .refclk (refclk),  .fbclk (1'b0),    .fboutclk ( ),
   .outclk ({outclk5,outclk4,outclk3,outclk2,outclk1,outclk0})
);
   
endmodule

