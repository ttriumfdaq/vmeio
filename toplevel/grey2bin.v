//grey to binary ... upper bit unchanged, each lower ^= prev ...
//   x[11:0] ->  {x[11], x[10]^x[11], x[9]^{x[10]^x[11]}, .... }
// 
// to speed up - can break ripple chain in middle, and
// calculate both possibilities for lower half of chain in advance
// (and use actual value to select correct choice when available)
//
module grey2bin #(parameter DWIDTH = 12) (input wire [DWIDTH-1:0] grey_in, output wire [DWIDTH-1:0] bin_out);

genvar i;

assign bin_out[DWIDTH-1] = grey_in[DWIDTH-1];
generate
for(i=DWIDTH-2; i>=0; i=i-1) begin : outbit
   assign bin_out[i] = grey_in[i] ^ bin_out[i+1];
end
endgenerate

endmodule
