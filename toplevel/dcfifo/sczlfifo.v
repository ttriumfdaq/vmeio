// must work when read every clk, or every N clks (and with immediate or delayed start)
//    reader is harder as have to re-read immediately - could use hints about next word, but is look-ahead fifo so easier?
//
// No invalid holes in register-list (or no improvement over normal fifo!) only need enough regs to cover mem latency
// continously shift rdint into valid-list every clk (follows rdrq thru memblk) + when valid-list[X] set, *Must* copy mem.Q into one of regs
//    which register bits:  keep count of Nvalid + use case + allow for read-shifts [copy to correct slot after any read-shift]
//    which fifo_q_valid_reg_bit:  depends on mem latency - have allowed up to 4clks [should match size of q_req]
//       rdreq_reg <-> mem.q latency: clk0 set rdreq_reg, clk1 rdaddr set, clk2 mem.q good [clk3 mem-reg good]
// 
// could add bypass-path direct to outputreg - do not set write and keep word count zero => reg has 3 sources: reg/mem.q/datain
//    will not be possible for dual clock (must go through dual-clock memory)
module sczlfifo #(parameter AWIDTH = 12,  parameter DATAREG = "CLOCK0", // **must be registered to work (clk:clk0 unused:clk1)
                  parameter DWIDTH = 32,  parameter DEVICE  = "Arria V" ) ( output wire debug,
   input  wire                clk,  input  wire      aclr,  output wire [AWIDTH-1:0] words,  output wire      error,
   input  wire [DWIDTH-1:0]  data,  input  wire     wrreq,  output wire               full,  output wire almostfull,
   output wire [DWIDTH-1:0]     q,  input  wire     rdreq,  output wire              empty,  output wire almostempty
); assign debug = fifo_q_valid_reg[2] ^ fifo_q_valid_reg[3];

assign almostfull   = (words > { {AWIDTH-5{1'h1}},4'hA} );
assign full         = fifo_full;
assign almostempty  = (valid_count < 3'h2); // only 1 word left - if reading on this clk, cannot set read for nxt clk 
assign q            =  q_reg[DWIDTH-1:0];
assign words        =  words_int + valid_count;
wire   read_request =  rdreq && !empty; // read_request only affects registers
assign error        = (rdreq && empty);
assign empty        = (valid_count == 3'h0);

 // attempt to keep register full - how to allow for latency on empty (will overrun while empty is about to clear)
wire [3:0] total_count = valid_count + pending_count; // 
wire rdreq_int = ( total_count < 4'h4 ) && (!fifo_empty);

reg [3:0] fifo_q_valid_reg /* synthesis keep */;  wire fifo_q_valid = fifo_q_valid_reg[1];
always @(posedge clk or posedge aclr) fifo_q_valid_reg <= (aclr) ? 4'h0 : {fifo_q_valid_reg[2:0],rdreq_int};

reg [2:0] valid_count;                       // valid count - lose one entry on rdreq, gain one on mem-valid
always @(posedge clk or posedge aclr) begin  // 
   if( aclr ) valid_count <= 3'h0;
   else       valid_count <= valid_count + (fifo_q_valid ? 3'h1 : 3'h0) - (read_request ? 3'h1 : 3'h0);
end

reg [2:0] pending_count;                     // pending count - gain one entry on rdreq_int, lose one on mem-valid
always @(posedge clk or posedge aclr) begin  //  **count is delayed 1clk - current count is pending + rdreq_int**
   if( aclr ) pending_count <= 3'h0;
   else       pending_count <= pending_count + (rdreq_int ? 3'h1 : 3'h0) - (fifo_q_valid ? 3'h1 : 3'h0);
end

reg [4*DWIDTH-1:0] q_reg /* synthesis keep */; 
always @(posedge clk or posedge aclr) begin
   if( aclr ) q_reg <= {4*DWIDTH{1'b0}};
   else begin
      q_reg <= read_request ? { {DWIDTH{1'b0}},q_reg[4*DWIDTH-1:DWIDTH]} : q_reg;
		if( fifo_q_valid ) begin
		   case ( (valid_count - ((read_request) ? 2'h1 : 2'h0)) ) // where to put this fifo_q
			2'h0:  q_reg[  DWIDTH-1:       0] <= fifo_q;
			2'h1:  q_reg[2*DWIDTH-1:  DWIDTH] <= fifo_q;
			2'h2:  q_reg[3*DWIDTH-1:2*DWIDTH] <= fifo_q;
			2'h3:  q_reg[4*DWIDTH-1:3*DWIDTH] <= fifo_q;
         endcase
		end
   end
end


wire [DWIDTH-1:0] fifo_q;  wire fifo_full, fifo_empty;  wire [AWIDTH-1:0] words_int /* synthesis keep */;
single_clk_fifo #(.DWIDTH(DWIDTH), .AWIDTH(AWIDTH), .DATAREG(DATAREG) ) scfifo (
   .clk(clk),     .aclr(aclr),         .words(words_int),
   .data(data),   .wrreq(wrreq),       .full(fifo_full),
   .q(fifo_q),    .rdreq(rdreq_int),   .empty(fifo_empty)
);
// attempted fifo improvements (all failed) ...
//    2 clock delay from read to #words changing - 1clk to change rdaddr, 1clk to update words from rdaddr
//    - can reduce to 1clk if #words look at rden and wren as well as addrs
//    - can reduce to 0clk if #words not registered
//    - also can do similar with full/empty
//      (for dual clk - can individually adjust rdaddr with rden and wraddr with wren)
//    assign words = wraddr - rdaddr + {11'h0,wren} - {11'h0,rden};
//    assign full = ((wraddr+{11'h0,wren}-{11'h0,rden}+12'h1) == rdaddr);
//    assign empty = ((wraddr+{11'h0,wren}-{11'h0,rden}     ) == rdaddr);
//    Problems ...
//       above -> hidden combinatorial loop if used to set rden
//          so do the ajustments externally where they are visible
//       also read protection at start prevents immediate read on write 
//       also can't read immediatly after write, as mem.q is not valid yet - so read will be of wrong data!
// 
// after write - have to wait couple of clks for output to be good (registered)
//    then can read - but then must wait couple clks for empty to respond to the read, before safe to read again
//       could continually read while buffer not full, and have fifo ignore reads on empty (and tell us)
// 
// current rdaddr,wraddr registered, words/empty/full derived from these => 1clk latency for counts, 2clk for data
// 

endmodule
