// everything is on read-clock (except write signals passed directly through to fifo)
module dczlfifo #(parameter AWIDTH = 12,  parameter DATAREG = "CLOCK1", // **must be registered to work (wrclk:clk0 rdclk:clk1)
                  parameter DWIDTH = 32,  parameter DEVICE  = "Arria V" ) ( input wire aclr,  output wire error,
   input  wire wrclk, input  wire [DWIDTH-1:0] data,  input wire wrreq,  output wire  wrfull,  output wire almostfull,   output wire [AWIDTH-1:0] wrwords,  
   input  wire rdclk, output wire [DWIDTH-1:0]    q,  input wire rdreq,  output wire rdempty,  output wire almostempty,  output wire [AWIDTH-1:0] rdwords,

   output wire [AWIDTH-1:0] fifo_words, output wire fifo_wrreq,  output wire  fifo_full,  output wire fifo_wren, output wire [AWIDTH-1:0] fifo_wraddr,
   output wire [DWIDTH-1:0] fifo_q,     output wire fifo_rdreq,  output wire fifo_empty,  output wire fifo_rden, output wire [AWIDTH-1:0] fifo_rdaddr
);
assign q            =  q_reg[DWIDTH-1:0];
assign almostfull   = ( wrwords > {{AWIDTH-5{1'h1}},4'hA} );
assign almostempty  = ( valid_count < 3'h2 ); // only 1 word left - if reading on this clk, cannot set read for nxt clk 
assign wrwords      =  fifo_wrwords;
assign rdwords      =  fifo_rdwords + valid_count;
assign wrfull       =  fifo_full;
assign rdempty      = ( valid_count == 3'h0 );
assign error        = ( rdreq && rdempty );

wire   read_request =  rdreq && !rdempty;        // read_request only affects registers

 // attempt to keep register full - how to allow for latency on empty (will overrun while empty is about to clear)
wire [3:0] total_count = valid_count + pending_count;
assign fifo_rdreq = ( total_count < 4'h4 ) && (!fifo_empty);

reg [3:0] fifo_q_valid_reg /* synthesis keep */;  wire fifo_q_valid = fifo_q_valid_reg[1];
always @(posedge rdclk or posedge aclr) fifo_q_valid_reg <= (aclr) ? 4'h0 : {fifo_q_valid_reg[2:0],fifo_rdreq};

reg [2:0] valid_count;                         // valid count - lose one entry on rdreq, gain one on mem-valid
always @(posedge rdclk or posedge aclr) begin  // 
   if( aclr ) valid_count <= 3'h0;
   else       valid_count <= valid_count + (fifo_q_valid ? 3'h1 : 3'h0) - (read_request ? 3'h1 : 3'h0);
end

reg [2:0] pending_count;                       // pending count - gain one entry on rdreq_int, lose one on mem-valid
always @(posedge rdclk or posedge aclr) begin  //  **count is delayed 1clk - current count is pending + rdreq_int**
   if( aclr ) pending_count <= 3'h0;
   else       pending_count <= pending_count + (fifo_rdreq ? 3'h1 : 3'h0) - (fifo_q_valid ? 3'h1 : 3'h0);
end

reg [4*DWIDTH-1:0] q_reg /* synthesis keep */; 
always @(posedge rdclk or posedge aclr) begin
   if( aclr ) q_reg <= {4*DWIDTH{1'b0}};
   else begin
      if( fifo_q_valid == 1'b0 ) begin // no new words to store - shift if reading
                   q_reg <= read_request ? {{1*DWIDTH{1'b0}},       q_reg[4*DWIDTH-1:1*DWIDTH]} :                          q_reg[4*DWIDTH-1:0*DWIDTH] ;
      end else begin
         case ( valid_count )         // new word to store - where to put it?
	        2'h0:  q_reg <= /* --------- cannot be read request if zero words valid ---------*/   {{3*DWIDTH{1'b0}},fifo_q                           };
	        2'h1:  q_reg <= read_request ? {{3*DWIDTH{1'b0}},fifo_q                           } : {{2*DWIDTH{1'b0}},fifo_q,q_reg[1*DWIDTH-1:0*DWIDTH]};
	        2'h2:  q_reg <= read_request ? {{2*DWIDTH{1'b0}},fifo_q,q_reg[2*DWIDTH-1:1*DWIDTH]} : {{1*DWIDTH{1'b0}},fifo_q,q_reg[2*DWIDTH-1:0*DWIDTH]};
	        2'h3:  q_reg <= read_request ? {{1*DWIDTH{1'b0}},fifo_q,q_reg[3*DWIDTH-1:1*DWIDTH]} : {                 fifo_q,q_reg[3*DWIDTH-1:0*DWIDTH]};
         endcase                // on read_request, fifo_q goes *into* #valid              otherwise         fifo_q goes after #valid
      end                       // also on read_req, #0 goes and #valid->#1 go below fifo_q
   end
end
//reg [4*DWIDTH-1:0] q_reg /* synthesis keep */; 
//always @(posedge rdclk or posedge aclr) begin
//   if( aclr ) q_reg <= {4*DWIDTH{1'b0}};
//   else begin
//      q_reg <= read_request ? { {DWIDTH{1'b0}},q_reg[3*DWIDTH-1:DWIDTH]} : q_reg;
//		if( fifo_q_valid ) begin
//		   case ( (valid_count - ((read_request) ? 2'h1 : 2'h0)) ) // where to put this fifo_q
//			2'h0:  q_reg[  DWIDTH-1:       0] <= fifo_q;
//			2'h1:  q_reg[2*DWIDTH-1:  DWIDTH] <= fifo_q;
//			2'h2:  q_reg[3*DWIDTH-1:2*DWIDTH] <= fifo_q;
//			2'h3:  q_reg[4*DWIDTH-1:3*DWIDTH] <= fifo_q;
//         endcase
//		end
//   end
//end

assign fifo_wrreq = wrreq;  assign fifo_words = fifo_wrwords;
wire [AWIDTH-1:0] fifo_wrwords; wire [AWIDTH-1:0] fifo_rdwords;
dual_clk_fifo #(.DWIDTH(DWIDTH), .AWIDTH(AWIDTH), .DATAREG(DATAREG) ) dc_fifo ( .aclr(aclr),
   .wrclk(wrclk),  .data(data),  .wrreq(fifo_wrreq),  .wrfull(fifo_full),   .wren(fifo_wren),  .wraddr(fifo_wraddr),  .wrwords(fifo_wrwords),
   .rdclk(rdclk),  .q(fifo_q),   .rdreq(fifo_rdreq),  .rdempty(fifo_empty), .rden(fifo_rden),  .rdaddr(fifo_rdaddr),  .rdwords(fifo_rdwords)
); 

endmodule
