// each side is almost independant, running on its own clk, each side maintains read/write address
//   but read/write positions need to be sent to to other side (asynchronous) to check if full/empty
//      since sending a multi-bit address through a synchroniser, need to grey-encode the value to avoid errors
//          bit transitions may each independantly be delayed or not by one clk
//             (do not allow transitions to spread by more than one clk, or grey-coding will still not be sufficient)
// 2^12 [4096] x 32bits
module dual_clk_fifo #(parameter AWIDTH = 12,  parameter DATAREG = "UNREGISTERED", 
                       parameter DWIDTH = 32,  parameter DEVICE  = "Arria V" ) (   input  wire aclr,        
   input  wire [DWIDTH-1:0] data, output reg [AWIDTH-1:0] wraddr,  output wire [AWIDTH-1:0] wrwords,  output wire almostfull,
	input  wire             wrclk, input wire               wrreq,  output wire                 wren,  output wire  wrfull,
   output wire [DWIDTH-1:0]    q, output reg [AWIDTH-1:0] rdaddr,  output wire [AWIDTH-1:0] rdwords,  output wire almostempty,
   input  wire             rdclk, input wire               rdreq,  output wire                 rden,  output wire rdempty
);
assign rdwords     = sync_wraddr - rdaddr;
assign wrwords     = wraddr - sync_rdaddr;
assign almostfull  = ( wrwords > {{AWIDTH-5{1'h1}},4'h0} );
assign almostempty = ( rdwords < {{AWIDTH-5{1'h1}},4'h3} );
assign wrfull      = ( (wraddr + {{AWIDTH-1{1'b0}},1'b1} ) == sync_rdaddr);
assign rdempty     = (  rdaddr                             == sync_wraddr);
assign wren        = wrreq && (! wrfull);
assign rden        = rdreq && (!rdempty);

dc_dpram #(.AWIDTH(AWIDTH), .DWIDTH(DWIDTH), .DATAREG(DATAREG), .DEVICE(DEVICE)) fifomem (
   .wrclock(wrclk), .wraddress(wraddr), .wren(wren), .data(data),
   .rdclock(rdclk), .rdaddress(rdaddr),                 .q(   q)
);

//reg [11:0]  wraddr;  // write side - each write incs write_ptr
always @(posedge wrclk or posedge aclr) begin
   if( aclr ) wraddr <= {AWIDTH{1'b0}};
	else wraddr <= ( wren ) ? wraddr + {{AWIDTH-1{1'b0}},1'b1} : wraddr;
end
//reg [11:0]  rdaddr;  // read side - each read incs read_ptr
always @(posedge rdclk or posedge aclr) begin
   if( aclr ) rdaddr <= {AWIDTH{1'b0}};
	else rdaddr <= ( rden ) ? rdaddr + {{AWIDTH-1{1'b0}},1'b1} : rdaddr;
end

// ===================================================================================
// to get word counts and full/empty flags, need to get other sides address value
// transfer to other clock: encode, resync to other clk, decode
// add multicycle paths to sdcfile to ignore timing failure errors on synchronisers

// wraddr -> sync_wraddr_in -> sync_wraddr_a -> sync_wraddr_grey -> sync_wraddr
reg  [AWIDTH-1:0] sync_wraddr_grey;  reg  [AWIDTH-1:0] sync_wraddr_a;
wire [AWIDTH-1:0] sync_wraddr_in;    wire [AWIDTH-1:0] sync_wraddr;
bin2grey #(.DWIDTH(AWIDTH)) wrenc( .bin_in(wraddr), .grey_out(sync_wraddr_in) );
always @(posedge rdclk) begin
   sync_wraddr_a    <= sync_wraddr_in;
	sync_wraddr_grey <= sync_wraddr_a;
end
grey2bin #(.DWIDTH(AWIDTH)) wrdec( .grey_in(sync_wraddr_grey), .bin_out(sync_wraddr) );

// rdaddr -> sync_rdaddr_in -> sync_rdaddr_a -> sync_rdaddr_grey -> sync_rdaddr
reg  [AWIDTH-1:0] sync_rdaddr_grey;  reg  [AWIDTH-1:0] sync_rdaddr_a;
wire [AWIDTH-1:0] sync_rdaddr_in;    wire [AWIDTH-1:0] sync_rdaddr;
bin2grey #(.DWIDTH(AWIDTH)) rdenc( .bin_in(rdaddr), .grey_out(sync_rdaddr_in) );
always @(posedge wrclk) begin
   sync_rdaddr_a    <= sync_rdaddr_in;
	sync_rdaddr_grey <= sync_rdaddr_a;
end
grey2bin #(.DWIDTH(AWIDTH)) rddec( .grey_in(sync_rdaddr_grey), .bin_out(sync_rdaddr) );

endmodule
