// transfer data-stream between two clocks
//    small rate differences are compensated by inserting/removing data
//       start with fifo half full (first few words read after reset are junk)
// read/write double-width memory every other clock
// rate matching adjustments are done on write side ... (have more complex version for read side )
//    normal case: save first data, write both data on 2nd clock (incount==1)
//    skip needed: on first  word: pause counter and overwrite next clk
//                 on second word: pause counter (don't store), overwrite next clk (and store)
//    insert     : on first  word: write both halves and store
//                 on second word: write+store as normal, set lsb of next pair
// can easily do repeated insertions/skips without any extra checks
// 
// control bit also complicates patterns - eth is now PAT=9'h050, PREV=9'h1BC
module ratematch_fifo #(
   parameter DWIDTH=32, parameter AWIDTH=8, parameter PATTERN=32'hBCFDFDFD, parameter PREV_PAT=32'hFFFFFFFF
) ( input wire reset,
   input  wire wrclk,   input  wire [DWIDTH-1:0] data_in,
   input  wire rdclk,   output wire [DWIDTH-1:0] data_out,

   output wire fifo_pattern,  output wire fifo_skip,  output wire fifo_insert, output wire fifo_incount,
	
   output wire [AWIDTH-1:0] fifo_wrwords,
   output wire fifo_wren, output wire [AWIDTH-1:0] fifo_wraddr, output wire [2*DWIDTH-1:0] fifo_memin,
   output wire fifo_rden, output wire [AWIDTH-1:0] fifo_rdaddr, output wire [2*DWIDTH-1:0] fifo_memq
);
assign fifo_pattern = (prev_ok && data_in == PATTERN);
assign fifo_skip    = skip;
assign fifo_insert  = insert;
assign fifo_incount = incount;
assign fifo_rdaddr  = read_addr;
assign fifo_wraddr  = write_addr;
assign fifo_wren    = wren;
assign fifo_rden    = 1'b1;
assign fifo_rden    = fifo_rden;
assign fifo_memq    = mem_q;
assign fifo_memin   = mem_in;
assign fifo_wrwords = wrwords;

/////  extra logic for ethernet etc where nultiple consecutive  bytes are matched/skipped/inserted ////////
reg [35:0] prev_data_in; always @ (posedge wrclk) prev_data_in <= {{36-DWIDTH{1'b0}},data_in};
wire prev_ok = (PREV_PAT == 32'hFFFFFFFF || prev_data_in == PREV_PAT);
reg skip_b, insert_b; always @ (posedge wrclk) begin skip_b <= skip_a; insert_b <= insert_a; end
//wire skip   /* synthesis keep */ = (PREV_PAT == 32'hFFFFFFFF) ? skip_a   : (skip_a   || skip_b   );
//wire insert /* synthesis keep */ = (PREV_PAT == 32'hFFFFFFFF) ? insert_a : (insert_a || insert_b );
wire skip   /* synthesis keep */ = skip_a;
wire insert /* synthesis keep */ = insert_a;
///////////////////////////////////////////////////////////////////////////////////////////////////////////

// will initially use up margin, then skip/insert on regular interval
//    may as well put setpoints close to centre so lots of margin left when needed
//   rdaddr starts at 0, wraddr should start at 4,8,16,32
//      then can set at 3,5  7,9 etc. 
//   UPDATE: too confusing to debug - use 2,6  6,10  etc.
wire [AWIDTH-1:0] mid   =       {1'b1,{AWIDTH-1{1'b0}}};
wire [AWIDTH-1:0] lolim = mid - {{AWIDTH-2{1'b0}},1'b1,1'b0}; // {{AWIDTH-1{1'b0}},1'b1}  {1'b0,{AWIDTH-2{1'b1}},1'b1};
wire [AWIDTH-1:0] hilim = mid + {{AWIDTH-2{1'b0}},1'b1,1'b0}; // {{AWIDTH-1{1'b0}},1'b1}  {1'b1,{AWIDTH-2{1'b0}},1'b1};

wire skip_a   = patok && above; // 21  22-31
wire insert_a = patok && below; // 10   0-9
wire above /* synthesis keep */ = (wrwords >= hilim );
wire below /* synthesis keep */ = (wrwords <= lolim );
wire patok /* synthesis keep */ =  prev_ok && (data_in == PATTERN);

wire wren   = (incount && !skip) || insert; // good for all 5 cases
wire [2*DWIDTH-1:0] mem_in = (insert && !incount) ? {data_in, data_in} : {data_in_save, data_in};

reg [DWIDTH-1:0] data_in_save;  reg incount;
reg [AWIDTH-1:0] write_addr; // read_addr must be < write_addr
always @ (posedge wrclk) begin
   if( reset ) begin
      incount <= 1'h0; write_addr <= mid; data_in_save <= {DWIDTH{1'b0}};
   end else begin
      incount      <= (skip||insert) ? incount : incount + 1'h1;
      write_addr   <= write_addr + {{AWIDTH-1{1'b0}},wren};
      data_in_save <= (skip) ? data_in_save : data_in;
   end
end

///////////////////////////////   read side   /////////////////////////////////////

reg [2*DWIDTH-1:0] data_out_reg; reg outcount;
reg [AWIDTH-1:0] read_addr; // read_addr must be < write_addr
always @ (posedge rdclk) begin
   if( reset ) begin outcount <= 1'h0; read_addr <= {AWIDTH{1'b0}}; data_out_reg <= {2*DWIDTH{1'b0}}; end
   else begin
      outcount    <= outcount + 1'h1;
      read_addr   <= outcount ? read_addr : (read_addr + { {AWIDTH-1{1'b0}},1'b1} );
	  data_out_reg <= outcount ? mem_q     :  data_out_reg;                 
   end 
end    
assign data_out = outcount   ? data_out_reg[DWIDTH-1:0] : data_out_reg[2*DWIDTH-1:DWIDTH];

// need to transfer read pointer to write clk to calculate #words in buffer
// rdaddr -> sync_rdaddr_in -> sync_rdaddr_a -> sync_rdaddr_grey -> sync_rdaddr
reg  [AWIDTH-1:0] sync_rdaddr_grey;  reg  [AWIDTH-1:0] sync_rdaddr_a;
wire [AWIDTH-1:0] sync_rdaddr_in;    wire [AWIDTH-1:0] sync_rdaddr;
bin2grey #(.DWIDTH(AWIDTH)) rdenc( .bin_in(read_addr), .grey_out(sync_rdaddr_in) );
always @(posedge wrclk) begin
   sync_rdaddr_a    <= sync_rdaddr_in;
   sync_rdaddr_grey <= sync_rdaddr_a;
end
grey2bin #(.DWIDTH(AWIDTH)) rddec( .grey_in(sync_rdaddr_grey), .bin_out(sync_rdaddr) );
wire [AWIDTH-1:0] wrwords = write_addr - (sync_rdaddr + { {AWIDTH-1{1'b0}},1'b1} );
// sync takes 2 clks and 1 write each 2 clks => wrwords always 1 behind - so add it
//   causes some strangeness after reset - but soon settles down
//   (Mem blk retains old contents until overwritten)
////////////////////////////////   memory block    ///////////////////////////////////////
wire [2*DWIDTH-1:0] mem_q;
dc_dpram #( .AWIDTH(AWIDTH), .DWIDTH(2*DWIDTH), .DATAREG("UNREGISTERED"), .DEVICE("Cyclone III")) fifo_mem (
   .wrclock(wrclk), .wraddress(write_addr), .wren(wren), .data(mem_in),
   .rdclock(rdclk), .rdaddress( read_addr),              .q(mem_q)
);  // .ram_block_type = "MLAB",

endmodule

