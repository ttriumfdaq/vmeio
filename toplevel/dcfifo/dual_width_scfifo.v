// simple fifo - unequal widths - default 1k x 512 [32 x 16k] for filter output fifo
module dual_width_scfifo   #(parameter AWIDTH =  9,  parameter DATAREG = "UNREGISTERED", // we register data in large shift-register, do not double-register
   parameter FACTOR = 8'h20, parameter DWIDTH = 32,  parameter DEVICE  = "Arria V" ) (
   input  wire                      clk,  input  wire  aclr,  output wire error,  output wire almostfull,    output wire almostempty,
   input  wire [FACTOR*DWIDTH-1:0] data,  input  wire wrreq,  output wire  full,  output wire wren,  output reg  [AWIDTH-1:0] wraddr,  output wire [AWIDTH-1:0] wrwords,  
   output wire        [DWIDTH-1:0]    q,  input  wire rdreq,  output wire empty,  output wire rden,  output reg  [AWIDTH-1:0] rdaddr,  output wire [      13:0] rdwords
   
);
assign q = q_reg[DWIDTH-1:0];
assign wrwords = words;
assign empty = (rdwords == 14'h0);

assign almostfull  = (  words >  {{AWIDTH-4{1'b1}},4'hb});
assign almostempty = (rdwords < 14'h003);
assign wren        = wrreq & (! full );
assign rden        = rdreq & (! empty );
assign error = (wrreq && full) || (rdreq && empty); // ??not sure

assign rdwords    = {words,5'h0} + q_count[5:0];

// with registered memory block, register automatically gets mem.q 2 clks after rdreq
//    here we have to manually load the register at the right time (can't wait for rdreq - too late!)
//    anytime we load reg, need to inc rdaddr for next value, and set q_count (both can be done at on clk as load reg)
wire mem_rden = (q_count[5:0] == 6'h0 || ((q_count[5:0] == 6'h1) && rden) ) && ((wraddr != rdaddr) || wren) &&
                  (mem_rden_del == 2'h0);
reg [1:0] mem_rden_del;  always @(posedge clk) mem_rden_del <= {mem_rden_del[0],mem_rden};
reg [1:0]     rden_del;  always @(posedge clk)     rden_del <= {    rden_del[0],    rden};

///fast fifo timing is read(3F,40,..) 1clk after writes(3e,3f), memq good 3clks after write(41,42,...)
// data on fifoq 1clk after memq good(42,43,...)
//    empty should be on un-delayed numbers
//    q_count also undelayed to keep mem_rden undelayed
//    q_reg control on delayed numbers
//    mem_rdaddr is delayed or will skip data

reg [FACTOR*DWIDTH-1:0] q_reg;
always @(posedge clk or posedge aclr) begin
   if(              aclr    ) q_reg <= {FACTOR*DWIDTH{1'b0}};
   else if( mem_rden_del[1] ) q_reg <=  mem_q;  // priority over rden if both set
   else if(     rden_del[1] ) q_reg <= {{DWIDTH{1'b0}},q_reg[FACTOR*DWIDTH-1:DWIDTH]};
   else                       q_reg <=  q_reg;
end
reg [7:0] q_count;
always @(posedge clk or posedge aclr) begin
   if(          aclr ) q_count <=  8'h0;
   else                q_count <= q_count - (rden ? 8'h1 : 8'h0) + (mem_rden ? FACTOR : 8'h0);
end  

wire [AWIDTH-1:0] words =   wraddr - rdaddr;
assign full             = ((wraddr+{{AWIDTH-1{1'b0}},1'b1}) == rdaddr);

wire [FACTOR*DWIDTH-1:0] mem_q;
// we register data above in large shift-register, do not double-register it
sc_dpram #(
   .AWIDTH(AWIDTH), .DWIDTH(FACTOR*DWIDTH), .DATAREG(DATAREG), .DEVICE(DEVICE)
) fifomem (.clock(clk), 
   .wraddress(wraddr), .wren(wren), .data(data),
   .rdaddress(rdaddr),                 .q(mem_q)
);

always @(posedge clk or posedge aclr) begin // write side - each write incs write_ptr
   if( aclr ) wraddr <= {AWIDTH{1'b0}};
	else       wraddr <= ( wren ) ? wraddr + {{AWIDTH-1{1'b0}},1'b1} : wraddr;
end

always @(posedge clk or posedge aclr) begin // read side - each read incs read_ptr
   if( aclr ) rdaddr <= {AWIDTH{1'b0}};
	else       rdaddr <= ( mem_rden_del[1] ) ? rdaddr + {{AWIDTH-1{1'b0}},1'b1} : rdaddr;
end

endmodule
