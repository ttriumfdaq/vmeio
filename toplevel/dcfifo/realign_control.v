module realign_control #(
   parameter NCHAN=4, parameter DWIDTH=9, parameter AWIDTH=4, parameter PATTERN=9'h17C
) ( input wire reset,
   input  wire [NCHAN-1:0] clk,   input  wire [NCHAN*DWIDTH-1:0] data_in,
                                  output wire [NCHAN*DWIDTH-1:0] data_out
);

reg aligned;
always @ (posedge clk[0]) begin
   if( reset ) aligned <= 1'b0;
   else        aligned <= aligned ? aligned : (pattern_ok == 4'hF);
end

wire [NCHAN-1:0] pattern_ok;
reg  [NCHAN-1:0] fifo_reset;
reg  [NCHAN-1:0] rd_n;
reg  [NCHAN-1:0] wr_n;
genvar i;
generate
   for(i=0; i<NCHAN; i=i+1) begin :al_chan
       assign pattern_ok[i] = data_out[DWIDTH*(i+1)-1:DWIDTH*i] == PATTERN;
       reg [AWIDTH-1:0] reset_count;
       always @ (posedge clk[0]) begin
         if( reset ) begin
            fifo_reset[i] <= 1'b1;  wr_n[i] <= 1'b0;   rd_n[i] <= 1'b0;
            reset_count   <= {1'b0,{AWIDTH-1{1'b1}}};
         end else begin
            fifo_reset[i] <= 1'b0;
            if( reset_count != {AWIDTH{1'b0}} ) begin
               reset_count <= (reset_count - {{AWIDTH-1{1'b0}},1'b1});
               wr_n[i] <= 1'b0;   rd_n[i] <= 1'b1;
            end else begin
               reset_count <= reset_count;
               wr_n[i] <= 1'b1;   rd_n[i] <= ( ~aligned && pattern_ok[i] ) ? 1'b0 : rd_n[i];
            end
         end
      end
      phasematch_fifo #( .DWIDTH(DWIDTH), .AWIDTH(AWIDTH) )  al_fifo_out ( .reset(fifo_reset[i]),
			.rdclk(clk[i]),  .Nrden(rd_n[i]), .data_out(data_out[DWIDTH*(i+1)-1:DWIDTH*i]),
         .wrclk(clk[i]),  .Nwren(wr_n[i]), .data_in (data_in [DWIDTH*(i+1)-1:DWIDTH*i])
      );
    end
endgenerate

endmodule

