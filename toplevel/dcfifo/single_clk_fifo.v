// 2^12 [4096] x 32bits  // DATAREG is "UNREGISTERED" or "CLOCK0"
module single_clk_fifo #(parameter AWIDTH = 12,  parameter DATAREG = "UNREGISTERED", 
                         parameter DWIDTH = 32,  parameter DEVICE  = "Arria V" ) (
   input  wire                 clk, input  wire aclr,  output wire  [AWIDTH-1:0]  words, 
   input  wire [DWIDTH-1:0]   data, input wire wrreq,  output wire  full,  output wire wren,  output reg  [AWIDTH-1:0] wraddr, output wire almostfull, 
   output wire [DWIDTH-1:0]      q, input wire rdreq,  output wire empty,  output wire rden,  output reg  [AWIDTH-1:0] rdaddr, output wire almostempty
);
assign almostfull  = ( words > {{AWIDTH-4{1'b1}},4'hb} );
assign almostempty = ( words < {{AWIDTH-4{1'b0}},4'h3} );
assign wren                      = wrreq & (! full);
assign rden /* synthesis keep */ = rdreq & (! empty);
//assign error = (wrreq && full) || (rdreq && empty);

assign words =   wraddr - rdaddr;
assign full  = ( (wraddr + {{AWIDTH-1{1'b0}},1'b1} ) == rdaddr);
assign empty = ( (wraddr                          ) == rdaddr);

sc_dpram #(
   .AWIDTH(AWIDTH), .DWIDTH(DWIDTH), .DATAREG(DATAREG), .DEVICE(DEVICE)
) fifomem ( .clock(clk), 
   .wraddress(wraddr), .wren(wren), .data(data),
   .rdaddress(rdaddr),                 .q(   q)
);

always @(posedge clk or posedge aclr) begin // write side - each write incs write_ptr
   if( aclr ) wraddr <= {AWIDTH{1'b0}};
   else       wraddr <= ( wren ) ? wraddr + {{AWIDTH-1{1'b0}},1'b1} : wraddr;
end

always @(posedge clk or posedge aclr) begin // read side - each read incs read_ptr
   if( aclr ) rdaddr <= {AWIDTH{1'b0}};
   else       rdaddr <= ( rden ) ? rdaddr + {{AWIDTH-1{1'b0}},1'b1} : rdaddr;
end

endmodule
