// not really a fifo - but if read remains a little behind write, behaves like one
// (also reset sequence is simpler than real fifo)
// using 4 word mem buffer with reads at 0,1,2,3,0 ... and writes at 2,3,0,1,2 ...
module phasematch_fifo #(parameter DWIDTH = 9) ( input wire reset,  // default for gx_interface
	input  wire wrclk, input wire Nwren, input  wire [DWIDTH-1:0] data_in,  
   input  wire rdclk, input wire Nrden, output wire [DWIDTH-1:0] data_out
);

reg [1:0] rdaddr; reg [1:0] wraddr;
always @ (posedge rdclk or posedge reset) begin
   if (reset)        rdaddr <= 2'h0;
   else if( !Nrden ) rdaddr <= rdaddr + 2'h1;
	else              rdaddr <= rdaddr;
end
always @ (posedge wrclk or posedge reset) begin
   if (reset)        wraddr <= 2'h2; // read_addr must be < write_addr
   else if( !Nwren ) wraddr <= wraddr + 2'h1;
	else              wraddr <= wraddr;
end

dc_dpram #(.AWIDTH(2), .DWIDTH(DWIDTH), .DATAREG("CLOCK1"), .DEVICE("Stratix IV")) fifomem (
   .wrclock(wrclk), .wraddress(wraddr), .wren(~Nwren), .data(data_in),
   .rdclock(rdclk), .rdaddress(rdaddr),                  .q(data_out)
);

endmodule
