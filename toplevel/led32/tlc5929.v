// driver for tlc5929

`default_nettype none
module tlc5929
  (
   input wire clock,
   input wire leds_update,
   input wire ctrl_update,
   input wire [15:0] leds,
   input wire [15:0] ctrl,

   output reg busy_out,
   output reg leds_update_out,
   output reg ctrl_update_out,
   output reg LED_LATCH,
   output reg LED_SCK,
   output reg LED_SDO
   );

   reg active;
   reg active_leds;
   reg active_ctrl;
   reg [5:0] counter;
   reg [5:0] phase;

   reg [16:0] shift;

   always_ff @(posedge clock) begin
      if (active) begin
         if (phase == 0) begin
            phase <= 1;
            LED_SCK <= 0;
            LED_SDO <= shift[16];
         end else if (phase == 1) begin
            phase <= 2;
            LED_SCK <= 1;
            LED_SDO <= shift[16];
         end else if (phase == 2) begin
            phase <= 3;
            LED_SCK <= 0;
            LED_SDO <= shift[16];
         end else if (phase == 3) begin
            LED_SCK <= 0;
            shift[16:0] <= {shift[15:0],1'b0};
            if (counter == 16) begin
               phase <= 4;
            end else begin
               counter <= counter + 1;
               phase <= 0;
            end
         end else if (phase == 4) begin
            LED_SCK <= 0;
            LED_SDO <= 0;
            LED_LATCH <= 0;
            leds_update_out <= 0;
            ctrl_update_out <= 0;
            phase <= 5;
         end else if (phase == 5) begin
            LED_SCK <= 0;
            LED_SDO <= 0;
            LED_LATCH <= 1;
            leds_update_out <= active_leds;
            ctrl_update_out <= active_ctrl;
            phase <= 6;
         end else if (phase == 6) begin
            LED_SCK <= 0;
            LED_SDO <= 0;
            LED_LATCH <= 0;
            leds_update_out <= 0;
            ctrl_update_out <= 0;
            phase <= 7;
         end else begin
            active <= 0;
            active_leds <= 0;
            active_ctrl <= 0;
            counter <= 0;
            phase <= 0;
            LED_SCK <= 0;
            LED_SDO <= 0;
            LED_LATCH <= 0;
            leds_update_out <= 0;
            ctrl_update_out <= 0;
         end
      end else if (leds_update) begin
         counter <= 0;
         active <= 1;
         active_leds <= 1;
         phase <= 0;
         shift <= {1'b0,leds};
         LED_SCK <= 0;
         LED_SDO <= 0;
         LED_LATCH <= 0;
         leds_update_out <= 0;
         ctrl_update_out <= 0;
         busy_out <= 1;
      end else if (ctrl_update) begin
         counter <= 0;
         active <= 1;
         active_ctrl <= 1;
         phase <= 0;
         shift <= {1'b1,ctrl};
         LED_SCK <= 0;
         LED_SDO <= 0;
         LED_LATCH <= 0;
         leds_update_out <= 0;
         ctrl_update_out <= 0;
         busy_out <= 1;
      end else begin
         counter <= 0;
         active <= 0;
         active_leds <= 0;
         active_ctrl <= 0;
         LED_SCK <= 0;
         LED_SDO <= 0;
         LED_LATCH <= 0;
         leds_update_out <= 0;
         ctrl_update_out <= 0;
         busy_out <= 0;
      end
   end

endmodule


