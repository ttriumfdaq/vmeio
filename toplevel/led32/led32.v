`default_nettype none
module led32
  (
   input wire clock,
   input wire        reset,
   input wire [31:0] data_async,
   
   output reg LED_BLANK,
   output reg LED_LATCH,
   output reg LED_SCK,
   output reg LED_SDO,
   output wire busy_out
   );

   reg [31:0] data1;
   reg [31:0] data2;
   reg [31:0] data_sync;

   always_ff @(posedge clock) begin
      data1 <= data_async;
      data2 <= data1;
      data_sync <= data2;
   end

   reg        xreset;
   reg        ctrl_update = 0;
   reg        led_update = 0;
   reg [31:0] led_data = 0;
   
   always_ff @(posedge clock) begin
      xreset <= reset;
      if (reset & ~xreset) begin
         ctrl_update <= 1;
         led_update <= 0;
      end else begin
         ctrl_update <= 0;
         if (led_update) begin
            led_update <= 1'b0;
         end else if (data_sync != led_data) begin
            led_update <= 1'b1;
            led_data   <= data_sync;
         end else begin
            led_update <= 1'b0;
         end
      end
   end

   assign LED_BLANK = 0;

//   led_display led_display
//     (
//      .sysclk          (clock),
//      .update_display  (led_update),
//      .led_data        (led_data),
//      .resetN          (~led_reset),
//      .LED_LATCH       (LED_LATCH),
//      .LED_SCK         (LED_SCK),
//      .LED_SDO         (LED_SDO)
//      );

   wire LED_LATCH_A;
   wire LED_SCK_A;
   wire LED_SDO_A;
   wire busy_a;
   wire busy_b;
   wire leds_update_a;
   wire ctrl_update_a;

   // from tlc5929.pdf page 22 "Function Control Data Bit Assignment", default power-on values
   wire [15:0] ctrl_data = { 1'b0,2'b11,2'b00,2'b11,2'b00,7'b1111111};
   
   tlc5929 a
     (
      .clock       (clock),
      .leds_update (led_update),
      .ctrl_update (ctrl_update),
      .leds        (led_data[15:0]),
      .ctrl        (ctrl_data),
      .LED_LATCH   (LED_LATCH_A),
      .LED_SCK     (LED_SCK_A),
      .LED_SDO     (LED_SDO_A),
      .leds_update_out (leds_update_a),
      .ctrl_update_out (ctrl_update_a),
      .busy_out        (busy_a)
      );

   wire LED_LATCH_B;
   wire LED_SCK_B;
   wire LED_SDO_B;
   wire LED_busy_b;
   
   tlc5929 b
     (
      .clock       (clock),
      .leds_update (leds_update_a),
      .ctrl_update (ctrl_update_a),
      .leds        (led_data[31:16]),
      .ctrl        (ctrl_data),
      .LED_LATCH   (LED_LATCH_B),
      .LED_SCK     (LED_SCK_B),
      .LED_SDO     (LED_SDO_B),
      .busy_out    (busy_b)
      );

   assign LED_LATCH = LED_LATCH_B;
   assign LED_SCK = LED_SCK_A | LED_SCK_B;
   assign LED_SDO = LED_SDO_A | LED_SDO_B;
   assign busy_out = busy_a | busy_b;

endmodule


