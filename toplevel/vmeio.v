// to check:  IACKINn ordering,

// add IO standards - check cyclone 5 book

// two 125Mhz crystals - one to LVDS_OSC1, other to XCVR_REFCLKL[1]
// cleaner clkin0 is fpga_clkout,  clkin1 is esata_clkref
//         four output clks to fpga - CLKIN1,2  XCVR_REFCLKL0,2

// xcvr max rate seems to be 3.125 for gx
`default_nettype none
module vmeio
  (
   input wire LVDS_OSC1,    // 125 MHz oscillator
   input wire LVDS_OSC1n,
   output wire FPGA_CLKOUT, // CLN_CLKIN0
   output wire FPGA_CLKOUTn,
   input wire FPGA_CLKIN1,  // CLN_CLKOUT1
   input wire FPGA_CLKIN1n,
   input wire FPGA_CLKIN2,  // CLN_CLKOUT2
   input wire FPGA_CLKIN2n,

   // There are 4 Dual esata, esata2-8 are the other 7 Xcvrs (as well as sfp)
   // esata1 has clk, trig - each can be in or out ...
   input  wire eSATA_TRIG,
   input  wire eSATA_TRIGn,
   input  wire eSATA_CLKmon,  // monitor on two signals, (clk also goes to cleaner)
   input  wire eSATA_CLKmonn,  // monitor on two signals, (clk also goes to cleaner)
   output wire eSATA_CLK_Out,
   output wire eSATA_CLK_OUT_EN,
   output wire eSATA_TRIG_OUT,
   output wire eSATA_TRIG_OUT_EN,
    
   // Front panel LEMO I/O LEDs, two blocks of 16 FP_LEDs
   // TI TLC5929DBQR SDI chained between them 
   output wire FP_LED_BLANK,
   output wire FP_LED_LATCH,
   output wire FP_LED_SCK,
   output wire FP_LED_SDI,
              
   // Front panel RGB LEDs: same driver as above, 12/16 used for 4*RGB
   // TI TLC9929DBQR
   output wire RGB_LED_BLANK,
   output wire RGB_LED_LATCH,
   output wire RGB_LED_SCK,
   output wire RGB_LED_SDI, // POWER/vrefb3an0, rgb led sdi from power? - FPGA vref?

   // these pins must be connected in the design.
   //input  wire  XCVR_REFCLK0L, // CLN_CLKOUT3
   //input  wire  XCVR_REFCLK1L, // LVDS_OSC2
   //input  wire  XCVR_REFCLK2L, // CLN_CLKOUT0
   //input  wire [7:0] GXB_RXL,  // 0:sfp, 1-7:esata
   //output wire [7:0] GXB_TXL,

   // MAC address EEPROM
   inout wire MAC_SCL,
   inout wire MAC_SDA,
    
   // SFP connector          
   inout wire SFP_SCL,
   inout wire SFP_SDA,
   output wire SFP_ModDet,
   input wire SFP_LOS,
   input wire SFP_TX_Fault,
   // output wire SFP_RateSelect, // grounded at SFP connector
   // output wire SFP_TX_Disable, // grounded at SFP connector

   // clock cleaner IDT 8T49N241-998NLGI
   input  wire CLN_nINT,
   output wire CLN_nRST,
   output wire CLN_nWR,
   inout  wire CLN_SCK,
   inout  wire CLN_SDA,
   inout  wire [3:0] CLN_GPIO,

   // NIM input and output, switchable
   input  wire [32:1] NIMIN,  // for reasons unknown board schematics count NIM input signals from 1 to 32.
   output wire [31:0] NIMOUT,
   output wire [31:0] NIMOUTn,

   // ECL/LVDS input
   input wire [15:0]  LVDS_RX,

   //
   // VME bus
   //
   // SN74VMEH22501ADGGR
   // SN74ABTE16246DGGR
   //

   // most signals are on the 11 2+8 devices:  2 have individual OE,IEn,  8 have DIR, OEn, LEn  [plus 2 clks]
   //    All 11 have OEn grounded, 9 have LEn attached to ALEn[5] DLE*[4] (and clocks ARCK,AWCK or DRCK,DWCK)
   //                              2 have no clocks and LEn tied to vcc           clocks control latch/DFF in each direction
   // IRQn_in/out are on a different device, which is always driving output?           ******Pin11 shown as LE NOT LEn *****
   // 
   // ** Following apulled DOWN - VME_MASTER, VME_MASTERn, VME_ARBITER, VME_SYSCTRL, VME_AOE, VME_DOE*, VME_DTACKOE, VME_IACKOE
   //                      also - VME_SYSFAIL_out, VME_BBSY_out, VME_BERR_out, VME_BR_out*
   //    VME_ENABLEn is PULLED UP
   
   output wire VME_AWCK,
   output wire VME_ARCK,
   output wire VME_DWCK,
   output wire VME_DRCK_L,
   output wire VME_DRCK_H,  // all outputs

   output wire VME_MASTER,     output wire VME_MASTERn,                                // mastern -> respn_out retryn_out
   input wire VME_RESPn_in,    output wire VME_RESPn_out,                              // master*N* enables output
   input wire VME_RETRYn_in,   output wire VME_RETRYn_out,                             // master*N* enables output

   inout wire  [5:0] VME_AM,                                                           // MASTER  sets direction of AM
   inout wire [31:0]  VME_A,
   //input wire VME_LWORDn, // connected to VME_A[0]
   output wire VME_AOE,
   output wire VME_ALEn,     // VME_AOE :DIR A, ***Lwordn comes in on serb-pin
   input wire VME_WRITEn_in,   output wire VME_WRITEn_out,                             // MASTER enables WRITEn_out
   inout wire [31:0]  VME_D,
   output wire VME_DOE_LW,
   output wire VME_DOE_HW,
   output wire VME_DLE_LWn,  // LE
   output wire VME_DLE_HWn,  // LE

   // VME AS and DS
   input wire VME_ASn_in,      output wire VME_ASn_out,                                // MASTER enables ASn_out
   input wire VME_DS0n_in,     output wire VME_DS0n_out,                               // MASTER enables VME_DS0n_out
   input wire VME_DS1n_in,     output wire VME_DS1n_out,                               // MASTER enables VME_DS1n_out

   // VME DTACK
   input wire VME_DTACKn_in,
   output wire VME_DTACKn_out,
   output wire VME_DTACK_OE, // OE enables out
                                                               
   input wire [7:1] VME_IRQn_in,  output wire [7:1] VME_IRQn_out,  output wire VME_ENABLEn,  // ENABLEn enables in+out of IRQ1-7
   input wire VME_IACKn_in,       input wire VME_IACKn_out,        output wire VME_IACKOE,   // OE enables out
   input wire VME_IACKINn,                                                                   // DIR fixed
   inout wire VME_IACKOUTn,                                              // MASTER sets direction                   VME_ENABLEn enables
   input wire [3:0] VME_BGINn,                                                               // DIR fixed
   inout wire [3:0] VME_BGOUTn,                                          // MASTER sets direction  **TYPO:BGOUTm1**  VME_ENABLEn enables
   input wire [4:0] VME_GAn,  input wire VME_GAPn,                                           // DIR fixed STRANGE ORDERING with IACKINn - Check

   input wire [3:0] VME_BRn_in,
   output wire [3:0] VME_BR_out,                        // these use 4 indiv chans: out goes to OE[data=GND]
   input wire VME_BBSYn_in,
   output wire VME_BBSY_out,                            //  same with this
   input wire VME_BERRn,
   output wire VME_BERR_out,                            //        and this [typo?]
   //input wire VME_SYSFAILn,
   output wire VME_SYSFAIL_out,
   input wire VME_BCLRn_in,
   output wire VME_BCLRn_out,
   output wire VME_ARBITER, // ARBITER enables output

   inout wire VME_SERA, // MASTER sets direction,  VME_ENABLEn enables
   //inout wire VME_SERB, // connected to VME_LWORDn aka VME_A[0]

   input wire VME_DIG_33V, // not sure what this is
   
   input wire VME_SYSRESETn /* in only */,    input wire VME_PBRESET,   // PBRESET comes from switch - when pressed, enables GND->sysrest_out
   input wire VME_SYSCLK_in /* in only */,    output wire SYSCLK_16M,   output wire VME_SYSCTRL,  //  sysctrl enables output of SYSCLK_16M to bus  
   input wire VME_ACFAILn  // input only
   );

   // LVDS receivers and drivers

   wire        clk_125MHz;
   wire        cln_clkout1;
   wire        cln_clkout2;

   alt_inbuf_diff clk_125MHz_inbuf( .i(LVDS_OSC1),   .ibar(LVDS_OSC1n),   .o(clk_125MHz));
   alt_inbuf_diff cln_clkout1_inbuf(.i(FPGA_CLKIN1), .ibar(FPGA_CLKIN1n), .o(cln_clkout1));
   alt_inbuf_diff cln_clkout2_inbuf(.i(FPGA_CLKIN2), .ibar(FPGA_CLKIN2n), .o(cln_clkout2));

   wire        esata_clock_async;
   wire        esata_trig_async;

   alt_inbuf_diff esata_clock_inbuf(.i(eSATA_CLKmon), .ibar(eSATA_CLKmonn), .o(esata_clock_async));
   alt_inbuf_diff esata_trig_inbuf( .i(eSATA_TRIG),   .ibar(eSATA_TRIGn),   .o(esata_trig_async));

   wire        cln_clkin0 = clk_125MHz; // clock cleaner CLKIN0
   
   alt_outbuf_diff cln_clkin0_outbuf( .o(FPGA_CLKOUT), .obar(FPGA_CLKOUTn), .i(cln_clkin0));

   wire [31:0] lemo_out;

   genvar      n;
   generate
      for (n=0; n<32; n++) begin: gen_lemo_outbuf
         alt_outbuf_diff lemo_outbuf(.o(NIMOUT[n]), .obar(NIMOUTn[n]), .i(lemo_out[n]));
      end
   endgenerate

//assign debug = vme_read_done ^ sfp_debug;

wire [47:0] mac_address = {48'h420000000401};

wire FMCX_TX, FMCX_RX;
wire sfp_debug;  wire reset_req;  wire [7:0] board_id;
///////////////////////////////////////////  PLL/Reset ///////////////////////////////////////////////////
wire RSTn, sys_locked, io_clk, sys_clk, fast_clk;   // sys:50 io:125 ppg:1 run:100                    
wire [31:0] poweron_time;  wire param_clk = sys_clk;
pll_reset pll_rst1 ( .inclk(LVDS_OSC1), .RSTn(RSTn), .autoreset( 1'b0 ),
    .sys_clk(sys_clk), .io_clk(io_clk),  .fast_clk(fast_clk),  .poweron_time(poweron_time), .reset_req(reset_req)
);

//wire init_debug; wire [47:0] mac_addr;
//module_init init ( .clk(io_clk), .reset(!RSTn),  .present(FMC1_PRESENT),  .debug(init_debug),
//   .progn(FMC1_CFG_PROGn), .cclk(FMC1_CFG_CCLK), .din(FMC1_CFG_DIN), .initn(FMC1_CFG_INITn), .done(FMC1_CFG_DONE),
//	.mac_clk(FMC1_C2M[0]), .mac_sdi(FMC1_M2C[0]), .mac_sdo(FMC1_C2M[1]), .mac_oen(FMC1_C2M[2]), .mac_addr(mac_addr)
//);

/////////////////////////////////////         VME             ///////////////////////////////////////////
wire reset = !RSTn;
//wire  [5:0] am_requested = 6'h9; // "001001" - a32 non-priviledged data access
//reg  [31:0] data_requested;  wire [31:0] readdata;   reg read_request; 
//reg  write_request;          wire readdatavalid;     wire write_ackn;
//always @ (posedge io_clk or posedge reset ) begin
//   if( reset ) begin
//	   write_request <= 1'b0;  read_request <= 1'b0;  data_requested <= 32'h0;
//	end else begin
//	   write_request  <= 1'b0;  read_request <= 1'b0;  
//      data_requested <= vme_request ? sfp_CmdDataOut[31:0] : data_requested;
//      write_request  <= ( vme_write_request ) ? 1'b1 : (( write_ackn       ) ? write_request : 1'b0);
//	   read_request   <= ( readdatavalid     ) ? 1'b0 : (( vme_read_request ) ? 1'b1 : read_request);
//	end
//end
//wire vme_request       = sfp_CmdDataOut[63] && (sfp_CmdDataOut[39:32] == 8'hff) && (sfp_CmdDataOut[61:48] == 14'h308);
//wire vme_write_request = vme_request && ~sfp_CmdDataOut[62];
//wire vme_read_request  = vme_request &&  sfp_CmdDataOut[62];
//wire vme_read_done     = read_request && readdatavalid;

//assign VME_DS0n_out = VME_DS1n_out;  wire vme_asn;
//vme_master vme (
//   .clk(io_clk),                .reset( !RSTn ),                 .enable(ENABLE_VME),
//   .vme_a(VME_A),               .vme_lwordn(VME_LWORDn),         .vme_am(VME_AM),
//   .vme_d(VME_D),               .vme_d_oe(VME_DOE),
//   .vme_asn(vme_asn),           .vme_dsn(VME_DS1n_out),          .vme_dtackn(VME_DTACKn_in),
//   .vme_writen(VME_WRITEn_out), .vme_berrn(VME_BERRn_in),
//
//   .vme_irqn(VME_IRQn_in),      .vme_iackn(VME_IACKn_out),       .vme_iackoutn(VME_IACKOUTn),
//	
//	.am_requested(am_requested),	    .address_requested(ppg_address),
//	.data_requested(data_requested),  .readdata(readdata),
//	.read_request(read_request),      .write_request(write_request),
//	.readdatavalid(readdatavalid),    .write_ackn(write_ackn)
//);
//reg as_dly1, as_dly2;
//always @ (posedge io_clk) begin VME_ASn_out <= as_dly1; as_dly1 <= as_dly2; as_dly2 <= vme_asn; end
//
//assign VME_BGOUTn   = VME_BGINn; /* tie together if vme-bus-request not implemented */
//assign VME_ALEn     =        1; assign VME_DLEn     =       1; // these are the only controllable LEn - all others tied high
//assign VME_MASTER   = !vme_asn; assign VME_MASTERn  = vme_asn; assign VME_DTACKOE = 0; assign VME_AOE    = !vme_asn;
//assign VME_ARBITER  =        0; assign VME_ENABLEn  =       1; assign VME_IACKOE  = 0; assign VME_SYSCTRL = 0;
//assign VME_BERR_out =        0; assign VME_BBSY_out =       0; assign VME_PBRESET = 0; assign VME_SYSFAIL_out = 0;

   assign VME_MASTER  = 0;
   assign VME_MASTERn = 0;
   assign VME_ALEn    = 1; // switch "latch enable" to transparent mode
   assign VME_DLE_LWn = 1; // switch "latch enable" to transparent mode
   assign VME_DLE_HWn = 1; // switch "latch enable" to transparent mode

/////////////////////////////       Digitiser Data/Param interfaces       /////////////////////////////////////
//wire [63:0] modpar_out /* synthesis keep */;  wire [15:0] chanmask;  wire [15:0] target_pkt_size;  wire [31:0] pulse_sim_ctrl;  wire [31:0] csr;
//wire [63:0] modpar_in = ( sfp_CmdDataOut[39:32] == 8'hFF ) ? sfp_CmdDataOut : 64'h0; // use chan=255 for this block  [port=15 already checked]
//param_io par ( .csr_in({8'h0}), // extra param block for parameters in this module (will overlap with sas15 if that exists!)
//   .clk(io_clk),  .reset( reset ),  .param_in(modpar_in),  .param_out(modpar_out), // param* must be syncd to clk (oneshot)
//	.csr(csr),  .chanmask(chanmask), .num_sample(target_pkt_size),  .pulser_ctrl(pulse_sim_ctrl) // was link monitor data
//); // drq/run/mst/reset all on csr ok - reset clrs all which is expected, run also clears lower-lvls mst (which is also ok)
//wire [63:0] sfp_CmdDataIn = ( vme_read_done ? {2'h3,14'h308,16'hFFFF,readdata} : modpar_out ); 

////////////////////////////    sfp Link    ///////////////////////////////////
//// to avoid junk network packets - filter param readback to either mst or sfp
//wire sfp_TxDataAck; wire [63:0] sfp_CmdDataOut;  wire sfp_datarequest;  wire sfp_ready;
//wire sfp_rstreq;  reg [31:0] sfp_rst_cnt; always @ (posedge io_clk) sfp_rst_cnt <= sfp_rst_cnt + (sfp_rstreq);
//gx_sfp sfp1( .modulelevel( 2'h2 ), .datarequest_out(sfp_datarequest),  //.net_halt(net_halt),
//   .clk(io_clk), .reset(reset),  .debug(sfp_debug), .board_id( board_id ), .target_pkt_size(target_pkt_size),
//	.RxDataIn (sas_RxDataOut[31:0]), .RxCtrlIn (sas_RxCtrlOut[0]),  .CmdDataIn( sfp_CmdDataIn),  // block4 is 48:63, 384-511
//	.TxDataOut(sas_TxDataIn [31:0]), .TxCtrlOut(sas_TxCtrlIn [0]),  .CmdDataOut(sfp_CmdDataOut), // signal is on [12], [103:96]
//   .param_clk(param_clk),  .mac_address(mac_address),  .xcvr_ready(sfp_ready),  .autoneg_reset(sfp_rstreq)
//);

// can't use lVDS_OSC1 for SFP refclk

//wire [31:0] sas_TxDataIn /* synthesis keep */;  wire [31:0] sas_RxDataOut /* synthesis keep */;
//wire  [3:0] sas_TxCtrlIn /* synthesis keep */;  wire  [3:0] sas_RxCtrlOut /* synthesis keep */;
//gx_cyclone5_block #(.NCHAN(1), .RATE(1)) block1 (  .reset( reset ), // sysclk supposedly 50Mhz
// //.xcvr_refclk(XCVR_REFCLKL),          .dataio_clk(io_clk),              .reconfig_clk(sys_clk),
//   .xcvr_refclk(LVDS_OSC1),             .dataio_clk(io_clk),              .reconfig_clk(sys_clk),
//   .rx_dataout(sas_RxDataOut[31:  0]),  .tx_ctrlen(  sas_TxCtrlIn[3: 0]), .FMCX_TX(GXB_TXL[0]),
//   .tx_datain(  sas_TxDataIn[31:  0]),  .rx_ctrldet(sas_RxCtrlOut[3: 0]), .FMCX_RX(GXB_RXL[0]), .channel_good(sfp_ready)        
//);
   
////////////////////////////   clock cleaner spi control
/////////////////////////////////
//wire [31:0] cln_txdata; wire cln_txready; wire cln_busy;  wire
//cln_done;  wire [7:0] cln_rxdata;
//lmk4828_init cln_init (
//   .clk(            ext_clk),  .reset( reset ),
//        .cln_dataout( cln_txdata),  .cln_readyout(cln_txready),
//        .cln_busyin(    cln_busy),
//        .cln_initdone(  cln_done),  .cln_syncn(     CLN_SYNCn),
//        .cln_reset(    CLN_RESET),  .cln_status(   CLN_STATUS)
//);
//wire [31:0] cln_txdata_in  = (cln_done) ? cln_rdtxdata  : cln_txdata;
//wire        cln_txready_in = (cln_done) ? cln_rdtxready : cln_txready;
//spi3 #( .HALFPERIOD(8'd7), .NUMDEV(1) ) cln_spi ( .debug(cln_dbg),
//   .clk(ext_clk), .reset( reset ),  .dev_in( cln_txdata_in[24] ),
//        .sdio( CLN_DATAUWIRE),  .sck(CLN_CLKUWIRE), .csn(CLN_LEUWIRE),
//   .data_in( cln_txdata_in[23:0]),  .dataready_in(cln_txready_in),
//        .data_out( cln_rxdata), .busy_out( cln_busy)
//);
//wire [31:0] cln_rdtxdata; wire cln_rdtxready;
//lmk4828_rdbk cln_rdbk (
//   .clk(            ext_clk),    .reset( reset ),
//        .cln_dataout( cln_rdtxdata),  .cln_readyout(cln_rdtxready),
//        .cln_busyin(    cln_busy),    .cln_initdone_in(cln_done)
//);

   // VME master signals
   
   assign VME_ASn_out = 1;
   assign VME_DS0n_out = 1;
   assign VME_DS1n_out = 1;
   assign VME_WRITEn_out = 1;
   
   // VME slave interface

   wire vme_led;
   wire vme_data_oe;
   wire vme_addr_oe;
   wire vme_am_oe = 0;
   wire vme_addressed;
   wire vme_dtack;
   wire vme_retry;
   wire vme_berr;

   wire [31:0] vme_d_in;
   wire [31:0] vme_d_out;

   wire [31:0] vme_a_in;
   wire [31:0] vme_a_out;

   wire [5:0] vme_am_in;
   wire [5:0] vme_am_out = 0;

   assign VME_DTACKn_out = ~vme_dtack;
   assign VME_DTACK_OE   =  vme_dtack;

   assign VME_DOE_LW = vme_data_oe;
   assign VME_DOE_HW = vme_data_oe;

   assign VME_AOE = vme_addr_oe;

   assign VME_AWCK = 0;
   assign VME_ARCK = 0;
   assign VME_DWCK = 0;
   assign VME_DRCK_L = 0;
   assign VME_DRCK_H = 0;

   assign VME_RESPn_out  = 1;
   assign VME_RETRYn_out = ~vme_retry;
   assign VME_BERR_out   = vme_berr;

   tristate32 vme_data
     (
      .dataio   ( VME_D ),
      .dataout  ( vme_d_in ),
      .datain   ( vme_d_out ),
      .oe       ( {32{vme_data_oe}} )
      );

   tristate32 vme_addr
     (
      .dataio   ( VME_A ),
      .dataout  ( vme_a_in ),
      .datain   ( vme_a_out ),
      .oe       ( {32{vme_addr_oe}} )
      );

   generate
      for (n=0; n<6; n++) begin: vme_am_tristate
         alt_iobuf vme_am_iobuf(.io(VME_AM[n]), .i(vme_am_in[n]), .o(vme_am_out[n]), .oe(vme_am_oe));
      end
   endgenerate

   wire [31:0] regs_A;
   wire [31:0] regs_D;
   wire [31:0] regs_D_read;
   wire        regs_D_ready;
   wire        regs_D_rdack;
   wire        regs_access;

   NVME NVME
     (
      // inputs
      .clk(clk_125MHz),
      .RSTn(1'b1),
      .VME_SYSRESETn(1'b1),
      .VME_ASn(VME_ASn_in),
      .VME_DSn({VME_DS1n_in,VME_DS0n_in}),
      .VME_AM(VME_AM),
      .VME_WRITEn(VME_WRITEn_in),
      //.VME_A({8'h00,vme_a_in[23:0]}),
      .VME_A(vme_a_in[31:0]),
      .VME_D(vme_d_in),
      .EXT_MOD_SEL(1'b1),
      .A24_MOD_SEL(8'h80),
      //.D_in(64'h1122334455667788),
      .D_in({regs_D_read,regs_D_read}),
      // outputs
      .reset(),
      .LED(vme_led),
      .addressed(vme_addressed),
      .data_oe(vme_data_oe),
      .addr_oe(vme_addr_oe),
      .VME_A_out(vme_a_out),
      .VME_D_out(vme_d_out),
      .VME_DTACK(vme_dtack),
      .VME_BERR(vme_berr),
      .VME_RETRY(vme_retry),
      .access(regs_access),
      .A(regs_A),
      .D(regs_D),
      .D_ready(regs_D_ready),
      .D_in_rdack(regs_D_rdack)
      );

   // chronobox registers

   wire [31:0] vmeio_lemo_out_reg;
   wire [31:0] vmeio_led_ctrl_reg;
   wire [31:0] vmeio_fp_led_out_reg;
   wire [31:0] vmeio_rgb_led_out_reg;

   // misc signals

   wire        vmeio_reconfig;

   vmeio_regs vmeio_regs
     (
      // control
      .clk(clk_125MHz),
      .reset(1'b0),
      // communications
      .addr(regs_A[17:2]),
      .read_strobe(1'b1),
      .read_data(regs_D_read),
      .read_rdack(regs_D_rdack),
      .write_data(regs_D),
      .write_strobe(regs_D_ready),
      // inputs
      .sof_revision_in(sof_timestamp),
      //.switches_in(SW),
      //.buttons_in(fpga_debounced_buttons),
      .flash_programmer_in(sfl_out_sig),
      .ecl_in_async(LVDS_RX[15:0]),
      .lemo_in_async(NIMIN[32:1]),
      //.gpio_in(GPIO_IN[17:0]),
      //.reg7_test_in(zero),
      //.reg8_scaler_data_in(cb_scaler_data),
      //.regF_input_num_in(cb_input_num),
      //.reg10_fifo_status(fifo_status),
      //.reg11_fifo_data(fifo_data),
      //.reg18_fc_ext_clk_100_counter(fc_ext_clk_100_counter),
      //.reg19_fc_ext_clk_ext_counter(fc_ext_clk_ext_counter),
      //.reg1A_fc_ts_clk_100_counter(fc_ts_clk_100_counter),
      //.reg1B_fc_ts_clk_ts_counter(fc_ts_clk_ts_counter),
      //.reg1C_ts_clk_pll_status(ts_clk_pll_status),
      // outputs
      .reg2_led_ctrl_out(vmeio_led_ctrl_reg),
      .reg3_fp_led_out(vmeio_fp_led_out_reg),
      // reg4 test register
      .reg5_rgb_led_out(vmeio_rgb_led_out_reg),
      .reg8_flash_programmer_out(sfl_in_sig),
      //.zero_scalers_out(cb_zero_scalers),
      //.latch_scalers_out(cb_latch_scalers),
      //.fifo_rdreq_out(fifo_rdreqx),
      //.ts_clk_pll_extswitch_out(ts_clk_pll_extswitch),
      //.scaler_addr_out(cb_scaler_addr),
      .reg11_lemo_out(vmeio_lemo_out_reg),
      //.regC_gpio_out(GPIO_OUT),
      //.regD_out_enable_out(cb_out_enable),
      .reset_pulse_out(),
      //.reconfig_pulse_out(vmeio_reconfig_pulse),
      .reconfig_out(vmeio_reconfig),
      //.sync_arm_out(cb_sync_arm),
      //.sync_out(cb_sync),
      //.reg12_out(cb_invert_a),
      //.reg13_out(cb_invert_b),
      //.reg14_out(cb_enable_le_a),
      //.reg15_out(cb_enable_le_b),
      //.reg16_out(cb_enable_te_a),
      //.reg17_out(cb_enable_te_b),
      //.reg1D_lemo_out_mux_out(cb_lemo_out_mux_ctrl),
      //.reg1E_cb_sync_a_out(cb_sync_mask[31:0]),
      //.reg1F_cb_sync_b_out(cb_sync_mask[63:32])
      );

   // chronobox main function

   localparam CB_N = (1+2+2+32+16); // 53
   localparam CB_N1 = CB_N-1;
   
   wire [CB_N1:0] cb_inputs = {clk_125MHz,cln_clkout2,cln_clkout1,esata_trig_async,esata_clock_async,NIMIN[32:1],LVDS_RX[15:0]};

   wire           cb_inputs_grand_ok = |cb_inputs;

   assign lemo_out = vmeio_lemo_out_reg;
   
   // LED display

   wire [31:0] fp_led_data;

   assign fp_led_data[0] = vme_led;
   assign fp_led_data[1] = vme_data_oe;
   assign fp_led_data[2] = vme_addr_oe;
   assign fp_led_data[3] = vme_addressed;
   assign fp_led_data[4] = vme_dtack;
   assign fp_led_data[5] = cb_inputs_grand_ok;
   assign fp_led_data[31:6] = 0;

   wire [31:0] rgb_led_data;

   assign rgb_led_data[15:0] = 0; // not connected
   assign rgb_led_data[16] = vme_led;
   assign rgb_led_data[17] = vme_led;
   assign rgb_led_data[18] = vme_led;
   assign rgb_led_data[19] = vme_led;
   assign rgb_led_data[20] = vme_data_oe;
   assign rgb_led_data[21] = vme_addr_oe;
   assign rgb_led_data[22] = vme_addressed;
   assign rgb_led_data[23] = 0; // not connected
   assign rgb_led_data[24] = vme_dtack;
   assign rgb_led_data[25] = vme_dtack;
   assign rgb_led_data[26] = vme_dtack;
   assign rgb_led_data[27] = vme_dtack;
   assign rgb_led_data[28] = vme_dtack;
   assign rgb_led_data[29] = vme_dtack;
   assign rgb_led_data[30] = vme_dtack;
   assign rgb_led_data[31] = vme_dtack;

   wire [31:0] fp_led_bus;

   always_comb begin
      case (vmeio_led_ctrl_reg[3:0])
         default: fp_led_bus = 0;
         0: fp_led_bus = fp_led_data;
         1: fp_led_bus = vmeio_fp_led_out_reg;
         2: fp_led_bus = lemo_out;
         3: fp_led_bus = NIMIN[32:1];
         4: fp_led_bus = LVDS_RX;
         5: fp_led_bus = lemo_out|NIMIN|LVDS_RX;
      endcase;
   end

   led32 fp_led32
     (
      .clock (clk_125MHz),
      .reset (vmeio_led_ctrl_reg[31]),
      .data_async (fp_led_bus),
      .LED_BLANK(FP_LED_BLANK),
      .LED_LATCH(FP_LED_LATCH),
      .LED_SCK(FP_LED_SCK),
      .LED_SDO(FP_LED_SDI)
      );

   wire [31:0] rgb_led_bus;

   always_comb begin
      case (vmeio_led_ctrl_reg[7:4])
         default: rgb_led_bus = 0;
         0: rgb_led_bus = rgb_led_data;
         1: rgb_led_bus = vmeio_rgb_led_out_reg;
      endcase;
   end

   led32 rgb_led32
     (
      .clock (clk_125MHz),
      .reset (vmeio_led_ctrl_reg[31]),
      .data_async (rgb_led_bus),
      .LED_BLANK(RGB_LED_BLANK),
      .LED_LATCH(RGB_LED_LATCH),
      .LED_SCK(RGB_LED_SCK),
      .LED_SDO(RGB_LED_SDI)
      );

   //
   // VME Flash programmer
   //
   
   wire [15:0] sfl_in_sig;
   wire [15:0] sfl_out_sig;
   
   epcs_programmer ep
     (
      .in(sfl_in_sig) ,	// input [15:0] in_sig
      .out(sfl_out_sig) // output [15:0] out_sig
      );

   // FPGA reboot

   wire        sysrst = 0;
   //wire        ru_clk = clk_15;
   reg         ru_clk = 0;

   reg [3:0]   ru_cnt;
   always_ff @(posedge clk_125MHz) begin
      ru_cnt <= ru_cnt + 4'd1;
      if (ru_cnt == 4'd0) begin
         ru_clk <= ~ru_clk;
      end
   end
   
   reg         ru_reconfig;
   reg 	       ru_reset;

   my_std_synchronizer sync_ru_reset(   .reset_n(1), .clk(ru_clk), .din(sysrst), .dout(ru_reset));
   my_std_synchronizer sync_ru_reconfig(.reset_n(1), .clk(ru_clk), .din(vmeio_reconfig), .dout(ru_reconfig));

   remote_update ru
     (
      .clock(ru_clk),
      .reset(ru_reset),
      .reconfig(ru_reconfig)
      );

   // SOF Timestamp
   
   wire [31:0] sof_timestamp;

   timestamp timestamp_inst(.data_out(sof_timestamp));

endmodule


