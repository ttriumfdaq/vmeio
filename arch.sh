#!/bin/csh

set base=( vmeio )

cd ..
set date=(`date | awk '{print $3$2substr($NF,3)}' `)
cp -rp ${base}_current ${base}_$date
cd ${base}_$date
./clean.sh
cd ..
zip -rv ${base}_$date.zip ${base}_$date
\rm -rf ${base}_$date
cp -p *.zip ~
echo *.zip
mv -i *.zip archive
