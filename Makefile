#! Makefile
#
# Before using the Makefile, run:
# /opt/intelFPGA/20.1/nios2eds/nios2_command_shell.sh

#SCRIPTDIR=$(dirname "$(readlink -f "$0")")
#PROJECT_PATH=$SCRIPTDIR/../hdl
#PROJECT_NAME="feam"
#PROJECT_REV="rev1"

QUARTUS_PGM := /daq/quartus/13.0sp1/quartus/bin/quartus_pgm

PROJECT_NAME := vmeio

#JTAG := -c \"$b\"
JTAG := 

all:: quartus

quartus:
	/usr/bin/time quartus_sh --flow compile $(PROJECT_NAME)
	-grep "Missing location assignment" output_files/$(PROJECT_NAME).fit.rpt
	-grep -i critical output_files/$(PROJECT_NAME).sta.rpt

jic:
	quartus_cpf -c $(PROJECT_NAME).cof

load_sof:
	$(QUARTUS_PGM) $(JTAG) -m JTAG -o "p;output_files/$(PROJECT_NAME).sof"

load_jic:
	$(QUARTUS_PGM) $(JTAG) -m JTAG -o "p;output_files/$(PROJECT_NAME).jic"

verify_jic:
	$(QUARTUS_PGM) $(JTAG) -m JTAG -o "v;output_files/$(PROJECT_NAME).jic"

# this does not work "Programming option E is illegal"
#read:
#	$(QUARTUS_PGM) $(JTAG) -m JTAG -o "e;read.bin;5CGXBC7B6"

clean:
	quartus_sh --clean $(PROJECT_NAME)

#end
